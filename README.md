# Hyper Diving

***

## Description
After finishing the main questline, and maybe having explored every system in the sector, there won't be a lot else to do. This mod adds a repeatable "gauntlet" gamemode, accessible through the gate network, that allows you to explore an infinite number of randomly generated systems, each one linking to the next in some way.

The randomly generated system themes can range from the default fringe stars, including quest exclusive ones (planet killer, alpha site, etc), to handcrafted and especially hard ones. 

## Visuals
TODO

## Installation and usage
To use this mod, simply download the project and unzip it in your Starsector mods directory, then enable it in the launcher.

## Support
You can report any problems at the [mod thread](link.com) in the Starsector forum.

## Roadmap
- Even more system themes
- Handcrafted special systems
- Difficulty setting
- Recoverable cryosleeper

## Contributing
For now, this is a solo project. However, feedback is appreciated.

## License
This work is licensed under a [Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License](http://creativecommons.org/licenses/by-nc-nd/4.0/).

## Project status
ongoing (25/05/23)
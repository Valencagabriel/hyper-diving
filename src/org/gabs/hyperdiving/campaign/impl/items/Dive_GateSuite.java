package org.gabs.hyperdiving.campaign.impl.items;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.CargoTransferHandlerAPI;
import com.fs.starfarer.api.campaign.impl.items.BaseSpecialItemPlugin;
import com.fs.starfarer.api.impl.campaign.GateEntityPlugin;
import com.fs.starfarer.api.ui.TooltipMakerAPI;
import com.fs.starfarer.api.util.Misc;

import java.awt.*;

public class Dive_GateSuite extends BaseSpecialItemPlugin {

    @Override
    public void createTooltip(TooltipMakerAPI tooltip, boolean expanded, CargoTransferHandlerAPI transferHandler, Object stackSource) {

        float opad = 10f;
        Color b = Misc.getPositiveHighlightColor();

        tooltip.addTitle(getName());

        if (!spec.getDesc().isEmpty()) {
            tooltip.addPara(spec.getDesc(), Misc.getTextColor(), opad);
        }

        addCostLabel(tooltip, opad, transferHandler, stackSource);

        tooltip.addPara("Right-click to allow gate integration", b, opad);
    }

    @Override
    public boolean hasRightClickAction() {
        return true;
    }

    @Override
    public boolean shouldRemoveOnRightClickAction() {
        return true;
    }

    @Override
    public void performRightClickAction() {
        // should be already set but, failsafe
        Global.getSector().getMemoryWithoutUpdate().set("$dive_playerCanUpgradeGate", true);

        Global.getSoundPlayer().playUISound(getSpec().getSoundId(), 1f, 1f);
        Global.getSector().getCampaignUI().getMessageDisplay().addMessage("Ready to upgrade an Active Gate");
    }
}

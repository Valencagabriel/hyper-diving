package org.gabs.hyperdiving;

import com.fs.starfarer.api.BaseModPlugin;
import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.SectorEntityToken;
import com.fs.starfarer.api.campaign.SpecialItemData;
import org.apache.log4j.Logger;
import org.gabs.hyperdiving.impl.campaign.ids.Gauntlet;
import org.gabs.hyperdiving.impl.campaign.missions.Dive_GauntletMission;
import org.gabs.hyperdiving.impl.campaign.Dive_GauntletHandler;
import org.gabs.hyperdiving.impl.campaign.procgen.themes.Dive_ThemeUtil;

public class Dive_HyperDiving extends BaseModPlugin {

    public static Logger log = Global.getLogger(Dive_HyperDiving.class);

    protected static Dive_GauntletHandler gauntletHandler;
    protected static Dive_ThemeUtil themeUtil;
    protected static Dive_GauntletMission gauntletMission;

    public Dive_HyperDiving() {
    }

    @Override
    public void onGameLoad(boolean newGame) {

        gauntletHandler = new Dive_GauntletHandler();

        if ( Global.getSector().getMemory().get("$dive_originGate") != null)
            gauntletHandler.setOriginGate( (SectorEntityToken) Global.getSector().getMemory().get("$dive_originGate"));

        gauntletHandler.forceFleeFromGauntlet();

        if ( Global.getSector().getMemory().get("$dive_gauntletMission") != null)
            gauntletMission = (Dive_GauntletMission) Global.getSector().getMemory().get("$dive_gauntletMission");
        else
            gauntletMission = new Dive_GauntletMission();

        gauntletHandler.setGauntletMission(gauntletMission);

        themeUtil = new Dive_ThemeUtil();

        //TODO remove after adding quest for the special item
        Object gotGift = Global.getSector().getMemoryWithoutUpdate().get(Gauntlet.RECEIVED_GIFT);
        if(gotGift == null){
            Global.getSector().getPlayerFleet().getCargo().addSpecial(new SpecialItemData(Gauntlet.GATE_SUITE,null),1f);
            Global.getSector().getMemoryWithoutUpdate().set(Gauntlet.RECEIVED_GIFT,true);
        }

    }

    @Override
    public void onEnabled(boolean wasEnabledBefore) {
    }

    @Override
    public void beforeGameSave() {
        Global.getSector().getMemory().set("$dive_originGate", gauntletHandler.getOriginGate());
    }

    public static Dive_GauntletHandler getGauntletHandler() {
        return gauntletHandler;
    }
    public static Dive_ThemeUtil getThemeUtil() {
        return themeUtil;
    }
    public static Dive_GauntletMission getGauntletMission() {
        return gauntletMission;
    }

}

package org.gabs.hyperdiving.impl.campaign.rulecmd;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.InteractionDialogAPI;
import com.fs.starfarer.api.campaign.rules.MemoryAPI;
import com.fs.starfarer.api.impl.campaign.rulecmd.BaseCommandPlugin;
import com.fs.starfarer.api.util.Misc;
import org.apache.log4j.Logger;
import org.gabs.hyperdiving.Dive_HyperDiving;
import org.gabs.hyperdiving.impl.campaign.Dive_GauntletHandler;

import java.util.List;
import java.util.Map;

public class Dive_GateDiveCMD extends BaseCommandPlugin {

    public static Logger log = Global.getLogger(Dive_GateDiveCMD.class);

    Dive_GauntletHandler gauntletHandler = Dive_HyperDiving.getGauntletHandler();

    @Override
    public boolean execute(String ruleId, InteractionDialogAPI dialog, List<Misc.Token> params, Map<String, MemoryAPI> memoryMap) {

        String command = params.get(0).getString(memoryMap);

        if (command == null) {
            return false;
        } else {
            if (command.equals("linkToGauntlet"))
                linkToGauntlet(dialog);
            else if (command.equals("unlinkGauntlet"))
                unlinkGauntlet(dialog);
            else if (command.equals("diveIntoGauntlet"))
                diveDeeperIntoTheGauntlet(dialog);
            else if (command.equals("returnToOrigin"))
                leaveGauntlet(dialog);
            else if (command.equals("generateNextSystem"))
                generateNextSystem(dialog);
            else if (command.equals("goToNextSystem"))
                diveDeeperIntoTheGauntlet(dialog);
            else if (command.equals("updateMission"))
                updateMission(dialog);

            log.info("Checking size of sector");
            //log.info("Small baseline: ?-126-?");
            //log.info("Normal baseline: ?-236-241-?");
            log.info("Current: " + Global.getSector().getStarSystems().size());

            return true;
        }
    }

    public void updateMission(InteractionDialogAPI dialog){
        gauntletHandler.updateMission(dialog);
        log.info("updating mission status");
    }

    public void linkToGauntlet(InteractionDialogAPI dialog){
        log.info("Creating gauntlet system");
        gauntletHandler.createGauntlet(dialog);
        log.info("Gauntlet system created");
    }

    public void diveDeeperIntoTheGauntlet(InteractionDialogAPI dialog){
        log.info("diving into gauntlet");
        dialog.dismiss();
        gauntletHandler.diveDeeperIntoTheGauntlet(dialog);
    }

    public void leaveGauntlet(InteractionDialogAPI dialog){
        log.info("leaving gauntlet");
        gauntletHandler.leaveGauntlet(dialog);
    }

    public void unlinkGauntlet(InteractionDialogAPI dialog){
        log.info("unlinking gauntlet");
        gauntletHandler.unlinkGauntlet(dialog);
    }

    public void generateNextSystem(InteractionDialogAPI dialog){
        log.info("generating next gauntlet level");
         gauntletHandler.generateNextSystem(dialog);
    }

}

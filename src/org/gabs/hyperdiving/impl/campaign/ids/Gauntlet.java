package org.gabs.hyperdiving.impl.campaign.ids;

public class Gauntlet {
    public static final int DEFAULT_DIFFICULTY = 1;
    public static final int MID_DEPTH = 5;
    public static final int BOSS_DEPTH = 10;

    public static final int PROBES_MIN = 1;

    public static final int REMNANT_MIN_FLEETS = 3;

    public static final String OVERRIDE_SYSTEM = "dive_overridesystem";
    public static final String OLD_SYSTEM = "dive_oldsystem";
    public static final String NEW_SYSTEM = "dive_newsystem";

    public static final String GATE_SUITE = "dive_gateSuite";

    public static final String GUIDE = "$dive_guide";
    public static final String DOCK = "$dive_dockyard_guide";
    public static final String STATION = "$dive_station_guide";

    public static final String FLED = "$dive_fled";
    public static final String INSIDE_GAUNTLET = "$dive_insideGauntlet";
    public static final String RECEIVED_GIFT = "$dive_receivedGift";
}

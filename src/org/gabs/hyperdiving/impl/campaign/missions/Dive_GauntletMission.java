package org.gabs.hyperdiving.impl.campaign.missions;

import com.fs.starfarer.api.campaign.InteractionDialogAPI;
import com.fs.starfarer.api.campaign.SectorEntityToken;
import com.fs.starfarer.api.campaign.StarSystemAPI;
import com.fs.starfarer.api.campaign.econ.MarketAPI;
import com.fs.starfarer.api.impl.campaign.ids.Tags;
import com.fs.starfarer.api.impl.campaign.missions.hub.HubMissionWithSearch;
import com.fs.starfarer.api.ui.TooltipMakerAPI;
import com.fs.starfarer.api.util.Misc;
import org.gabs.hyperdiving.Dive_HyperDiving;
import org.gabs.hyperdiving.impl.campaign.Dive_GauntletHandler;
import org.gabs.hyperdiving.impl.campaign.ids.Gauntlet;
import org.gabs.hyperdiving.impl.campaign.procgen.themes.Dive_ThemeEnum;

import java.awt.*;

public class Dive_GauntletMission extends HubMissionWithSearch {

    protected static boolean missionActive = false;

    protected StarSystemAPI system;
    protected SectorEntityToken guide;
    protected Dive_ThemeEnum currentTheme;

    public void updateTheme(Dive_ThemeEnum currentTheme) {
        this.currentTheme = currentTheme;
    }

    public static enum Stage {
        FIND_GUIDE,
        DEFEAT_BOSS,
        GO_TO_GATE,
        GO_TO_SYSTEM,
        CHECKPOINT,
        FLED,
        COMPLETED,
    }

    public void updateMission(InteractionDialogAPI dialog){
        checkStageChangesAndTriggers(dialog, null);
    }

    public void abandon(InteractionDialogAPI dialog){
        //this.abort();
        //this.endImmediately();
        //this.setImportant(false);
        //this.shouldRemoveIntel();
        //this.reportRemovedIntel();
        //this.getGlobalMemory().unset("$dive_gauntletMission");
        //this.currentStage = Stage.FLED;
        //this.runTriggers();
    }

    public void setupNextGauntlet(SectorEntityToken guide){
        this.guide = guide;
        makeImportant(this.guide, Gauntlet.GUIDE, Stage.FIND_GUIDE);
    }

    public Dive_GauntletMission createGauntletMission(StarSystemAPI gauntletSystem, SectorEntityToken originGate, SectorEntityToken guide, Dive_ThemeEnum currentTheme){
        this.system = gauntletSystem;
        this.guide = guide;
        this.currentTheme = currentTheme;

        if(!create(originGate.getMarket(),false)){
            //if (this.currentStage != Stage.FIND_GUIDE)
                //this.currentStage = Stage.FIND_GUIDE;
        }

        return this;
    }

    //TODO add mission position
    //TODO add icon/image to mission description

    @Override
    protected boolean create(MarketAPI marketAPI, boolean b) {
        if (!setGlobalReference("$dive_gauntletMission")) {
            missionActive = true;
            return false;
        }
        else
            missionActive = false;

        this.currentStage = Stage.FIND_GUIDE;
        this.stageTransitionsRepeatable = true;
        setNoAbandon();
        setIconName("dive_missions", "dive_gate");
        addTag(Tags.INTEL_GATES);

        setStartingStage(Stage.FIND_GUIDE);
        //addSuccessStages(Dive_GauntletMission.Stage.COMPLETED);
        //addFailureStages(Stage.FLED);

        makeImportant(this.guide, Gauntlet.GUIDE, Stage.FIND_GUIDE);

        setStageOnGlobalFlag(Stage.FIND_GUIDE, "$dive_findGuide");
        setStageOnGlobalFlag(Stage.GO_TO_GATE, "$dive_gotCoordinates");
        setStageOnGlobalFlag(Stage.GO_TO_SYSTEM, "$dive_canProceed");
        setStageOnGlobalFlag(Stage.FLED, Gauntlet.FLED);

        return true;

    }

    @Override
    public String getBaseName() {
        switch (Stage.valueOf(currentStage.toString())){
            case FIND_GUIDE:
            case GO_TO_SYSTEM:
                if(Dive_HyperDiving.getGauntletHandler().getStarSystemHandler() != null && Dive_HyperDiving.getGauntletHandler().getStarSystemHandler().getOverrideGuide())
                    switch (Dive_HyperDiving.getGauntletHandler().getStarSystemHandler().getCurrentSubtheme()){
                        case RESEARCH:
                            return "Coordinates Recovery - Research Station";
                        case MINING:
                            return "Coordinates Recovery - Mining Station";
                        case SCAVENGER:
                            return "Coordinates Recovery - Dockyard";
                    }
                return "Coordinates Recovery - Derelict";
            case GO_TO_GATE:
                return "Use the new coordinates";
            case FLED:
                return "Gate network connection severed";
            case DEFEAT_BOSS:
                return "Find the interference source";
            default:
                return "Hyper diving";
        }
    }

    //TODO multiple types of derelict guides
    @Override
    public void addDescriptionForNonEndStage(TooltipMakerAPI info, float width, float height) {
        float opad = 10f;
        Color h = Misc.getHighlightColor();

        String locationInfo = "";

        String extra = "";
        switch (currentTheme){
            case MEMENTO:
                extra = "This system is strangely familiar.";
                break;
            case BOSS:
                extra = "A system-wide interference is affecting your sensors.";
                break;
        }

        String context = "derelict ship.";
        if(Dive_HyperDiving.getGauntletHandler().getStarSystemHandler() != null && Dive_HyperDiving.getGauntletHandler().getStarSystemHandler().getOverrideGuide())
            switch (Dive_HyperDiving.getGauntletHandler().getStarSystemHandler().getCurrentSubtheme()){
                case RESEARCH:
                    context = "Research Station.";
                    break;
                case MINING:
                    context = "Mining Station.";
                    break;
                case SCAVENGER:
                    context = "Dockyard.";
                    break;
            }

        switch (Stage.valueOf(currentStage.toString())){
            case FIND_GUIDE:
                info.addPara("Retrieve the next gate coordinates from a " + context, opad);
                if (extra.length()>0)
                    info.addPara(extra, opad);
                break;
            case GO_TO_GATE:
            case GO_TO_SYSTEM:
                info.addPara("Within the recovered logs you found the coordinates of a new gate.", opad);
                info.addPara("You can now go deeper into the gate network.", opad);

                if (Dive_HyperDiving.getGauntletHandler().getGauntletSystem().getStar() != null){
                    String[] starType = Dive_HyperDiving.getGauntletHandler().getGauntletSystem().getStar().getTypeId().split("_");
                    if (starType[0].equals("star") )
                        if (starType.length == 2)
                            locationInfo = locationInfo.concat("Further analysis of the logs indicate that the system star classification is: ").concat(starType[1]).concat(".");
                        else if(starType.length == 3)
                            locationInfo = locationInfo.concat("Further analysis of the logs indicate that the system star classification is: ").concat(starType[1]).concat(" ").concat(starType[2]).concat(".");
                        else
                            locationInfo = locationInfo.concat("Further analysis of the logs indicate that the system has a ").concat(starType[0]).concat(" ").concat(starType[1]);
                    info.addPara(locationInfo, opad);
                }
                break;
            case FLED:
                info.addPara("You severed your connection to the gate network and cannot go back to where you were.", opad);
                break;
            default:
                info.addPara("Hyper diving placeholder", opad);
                break;
        }
    }

    public static boolean isActive() {
        return missionActive;
    }
}

package org.gabs.hyperdiving.impl.campaign;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.*;
import com.fs.starfarer.api.campaign.comm.IntelInfoPlugin;
import com.fs.starfarer.api.campaign.listeners.ListenerUtil;
import com.fs.starfarer.api.impl.campaign.GateEntityPlugin;
import com.fs.starfarer.api.impl.campaign.ids.Abilities;
import com.fs.starfarer.api.impl.campaign.intel.misc.GateIntel;
import com.fs.starfarer.api.impl.campaign.procgen.Constellation;
import com.fs.starfarer.api.impl.campaign.procgen.StarSystemGenerator;
import org.gabs.hyperdiving.Dive_HyperDiving;
import org.gabs.hyperdiving.impl.campaign.ids.Gauntlet;
import org.gabs.hyperdiving.impl.campaign.missions.Dive_GauntletMission;
import org.gabs.hyperdiving.impl.campaign.procgen.Dive_GenericParam;
import org.gabs.hyperdiving.impl.campaign.procgen.Dive_StarSystemHandler;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Dive_GauntletHandler {

    protected Random random;

    Dive_StarSystemHandler starSystemHandler;

    protected InteractionDialogAPI dialog;

    protected SectorEntityToken originGate;
    protected SectorEntityToken gauntletGate;

    protected StarSystemGenerator.CustomConstellationParams systemParams;
    protected Constellation gauntletConstellation = null;
    protected StarSystemAPI oldGauntletSystem = null;
    protected StarSystemAPI gauntletSystem = null;

    protected int currentDepth = 1;
    protected int currentDifficulty = 1;

    Dive_GauntletMission gauntletMission;

    public StarSystemAPI createGauntlet(InteractionDialogAPI dialog){

        this.dialog = dialog;
        this.originGate = dialog.getInteractionTarget();

        currentDepth = 1;
        currentDifficulty = Gauntlet.DEFAULT_DIFFICULTY;

        Global.getSector().getMemoryWithoutUpdate().set(Gauntlet.FLED,false);

        systemParams = Dive_GenericParam.systemParams();
        systemParams.location = originGate.getStarSystem().getLocation();
        starSystemHandler = new Dive_StarSystemHandler(systemParams,currentDepth, originGate);

        handleGauntletGeneration(dialog);

        createGauntletMission(dialog);

        return gauntletSystem;
    }

    public void generateNextSystem(InteractionDialogAPI dialog){
        this.dialog = dialog;

        currentDepth++;
        if (currentDepth % Gauntlet.BOSS_DEPTH == 0)
            currentDifficulty++;

        handleGauntletGeneration(dialog);
        gauntletMission.updateTheme(starSystemHandler.getCurrentTheme());

    }

    protected void handleGauntletGeneration(InteractionDialogAPI dialog){

        starSystemHandler.setCurrentDepth(currentDepth);

        if (this.gauntletSystem != null){
            String temp = this.gauntletSystem.getName();
            this.gauntletSystem.setBaseName(Gauntlet.OLD_SYSTEM);
            this.gauntletSystem.setName(temp);
        }

        if (currentDepth == 1)
            this.gauntletSystem = starSystemHandler.handleSystemGeneration(originGate.getStarSystem().getLocation(),true);
        else
            this.gauntletSystem = starSystemHandler.handleSystemGeneration(originGate.getStarSystem().getLocation(),false);

        this.gauntletConstellation = this.gauntletSystem.getConstellation();
        this.gauntletGate = starSystemHandler.getGauntletGate();

        handleJumpPoints();
    }

    protected void createGauntletMission(InteractionDialogAPI dialog ){
        gauntletMission.getChanges().clear();
        gauntletMission = Dive_HyperDiving.getGauntletMission().createGauntletMission(gauntletSystem,originGate, starSystemHandler.getGuide(), starSystemHandler.getCurrentTheme());

        if (!gauntletMission.isActive()){
            RuleBasedDialog rbd = ((RuleBasedDialog) dialog.getPlugin());
            rbd.updateMemory();
            gauntletMission.accept(dialog,rbd.getMemoryMap());
        }
        else
            gauntletMission.updateMission(dialog);

    }

    protected void handleJumpPoints(){
        for(SectorEntityToken jumpPoint: gauntletSystem.getJumpPoints())
            jumpPoint.addTag("dive_unstableJump");
    }


    public void genericGateJump(SectorEntityToken origin, SectorEntityToken destination, boolean fastTransition){

        CampaignFleetAPI player = Global.getSector().getPlayerFleet();
        Global.getSector().setPaused(false);

        JumpPointAPI.JumpDestination dest = new JumpPointAPI.JumpDestination(destination, null);
        Global.getSector().doHyperspaceTransition(player, origin, dest, fastTransition?2.0F:0.5F);

        GateEntityPlugin plugin;
        if (destination.getCustomPlugin() instanceof GateEntityPlugin) {
            plugin = (GateEntityPlugin) destination.getCustomPlugin();
            plugin.showBeingUsed(25.0F);
        }

        if (origin.getCustomPlugin() instanceof GateEntityPlugin) {
            plugin = (GateEntityPlugin)origin.getCustomPlugin();
            plugin.showBeingUsed(25.0F);
        }
        ListenerUtil.reportFleetTransitingGate(Global.getSector().getPlayerFleet(), origin, destination);
    }

    public void diveDeeperIntoTheGauntlet(InteractionDialogAPI dialog){
        removeGateIntel();
        if(Global.getSector().getStarSystem(Gauntlet.OLD_SYSTEM) != null)
            Global.getSector().getStarSystem(Gauntlet.OLD_SYSTEM).setBaseName(Gauntlet.OVERRIDE_SYSTEM);
        gauntletMission.setupNextGauntlet(starSystemHandler.getGuide());
        genericGateJump(dialog.getInteractionTarget(),gauntletGate,false);
        setInsideGauntlet();
    }

    protected void setInsideGauntlet(){
        Global.getSector().getMemoryWithoutUpdate().set("$dive_insideGauntlet", true);
        Global.getSector().getPlayerFleet().getAbility(Abilities.TRANSVERSE_JUMP).setCooldownLeft(Float.MAX_VALUE);
    }

    protected void setOutsideGauntlet(){
        Global.getSector().getMemoryWithoutUpdate().set("$dive_insideGauntlet", false);
        Global.getSector().getPlayerFleet().getAbility(Abilities.TRANSVERSE_JUMP).setCooldownLeft(0);
    }

    protected void removeGateIntel(){
        List<IntelInfoPlugin> gateIntel = Global.getSector().getIntelManager().getIntel(GateIntel.class);
        IntelInfoPlugin toRemove = null;
        for (IntelInfoPlugin intel : gateIntel)
            if (((GateIntel) intel).getGate().getTags().contains("dive_gauntletGate"))
                toRemove = intel;
        if (toRemove != null)
            Global.getSector().getIntelManager().removeIntel(toRemove);
    }

    public void unlinkGauntlet(InteractionDialogAPI dialog){
        if(Global.getSector().getStarSystem(Gauntlet.OLD_SYSTEM) != null)
            Global.getSector().getStarSystem(Gauntlet.OLD_SYSTEM).setBaseName(Gauntlet.OVERRIDE_SYSTEM);

        Global.getSector().getMemoryWithoutUpdate().set("$dive_findGuide",false);
        Global.getSector().getMemoryWithoutUpdate().set("$dive_fled",true);
        gauntletMission.updateMission(dialog);
        removeGateIntel();
    }

    public void leaveGauntlet(InteractionDialogAPI dialog){
        if(Global.getSector().getStarSystem(Gauntlet.OLD_SYSTEM) != null)
            Global.getSector().getStarSystem(Gauntlet.OLD_SYSTEM).setBaseName(Gauntlet.OVERRIDE_SYSTEM);

        Global.getSector().getMemoryWithoutUpdate().set("$dive_findGuide",false);
        Global.getSector().getMemoryWithoutUpdate().set("$dive_fled",true);
        dialog.dismiss();
        gauntletMission.updateMission(dialog);
        genericGateJump(dialog.getInteractionTarget(),originGate,false);
        removeGateIntel();
        //deleteGauntlet(dialog);
        setOutsideGauntlet();
    }

    public void updateMission(InteractionDialogAPI dialog){
        if (gauntletMission != null)
            gauntletMission.updateMission(dialog);
    }

    public void forceFleeFromGauntlet() {
        if(Global.getSector().getStarSystem(Gauntlet.OLD_SYSTEM) != null)
            Global.getSector().getStarSystem(Gauntlet.OLD_SYSTEM).setBaseName(Gauntlet.OVERRIDE_SYSTEM);

        CampaignFleetAPI player = Global.getSector().getPlayerFleet();

        player.setNoEngaging(5.0F);

        Global.getSector().getMemoryWithoutUpdate().set("$dive_findGuide",false);
        Global.getSector().getMemoryWithoutUpdate().set("$dive_gotCoordinates",false);
        Global.getSector().getMemoryWithoutUpdate().set("$dive_canProceed",false);
        Global.getSector().getMemoryWithoutUpdate().set("$dive_fled",true);

        if (originGate != null)
            originGate.getMemoryWithoutUpdate().set("$dive_linked", false);

        if(Global.getSector().getMemoryWithoutUpdate().get("$dive_insideGauntlet") != null && (Boolean) Global.getSector().getMemoryWithoutUpdate().get("$dive_insideGauntlet")){

            if (originGate == null)
                originGate = Global.getSector().getStarSystem("corvus").getEntitiesWithTag("gate").get(0);

            genericGateJump(player,originGate,true);
            removeGateIntel();
            setOutsideGauntlet();

        }
        player.clearAssignments();
        player.addAssignment(FleetAssignment.GO_TO_LOCATION, originGate, 1.0F);
    }


    //Getters
    public StarSystemAPI getGauntletSystem() {
        return gauntletSystem;
    }

    public SectorEntityToken getOriginGate() {
        return originGate;
    }

    public SectorEntityToken getGauntletGate() {
        return gauntletGate;
    }

    public Constellation getGauntletConstellation() {
        return gauntletConstellation;
    }

    public int getCurrentDifficulty() {
        return currentDifficulty;
    }

    public Dive_GauntletMission getGauntletMission() {
        return gauntletMission;
    }

    public Dive_StarSystemHandler getStarSystemHandler() {
        return starSystemHandler;
    }

    //Setters
    public void setDialog(InteractionDialogAPI dialog) {
        this.dialog = dialog;
    }

    public void setOriginGate(SectorEntityToken originGate) {
        this.originGate = originGate;
    }

    public void setGauntletMission(Dive_GauntletMission gauntletMission){
        this.gauntletMission = gauntletMission;
    }

}


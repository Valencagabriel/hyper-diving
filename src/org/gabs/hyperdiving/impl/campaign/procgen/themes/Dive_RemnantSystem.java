package org.gabs.hyperdiving.impl.campaign.procgen.themes;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.*;
import com.fs.starfarer.api.characters.PersonAPI;
import com.fs.starfarer.api.combat.BattleCreationContext;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import com.fs.starfarer.api.fleet.FleetMemberType;
import com.fs.starfarer.api.impl.campaign.FleetEncounterContext;
import com.fs.starfarer.api.impl.campaign.FleetInteractionDialogPluginImpl;
import com.fs.starfarer.api.impl.campaign.fleets.FleetFactoryV3;
import com.fs.starfarer.api.impl.campaign.ids.*;
import com.fs.starfarer.api.impl.campaign.procgen.themes.*;
import com.fs.starfarer.api.util.Misc;
import com.fs.starfarer.api.util.WeightedRandomPicker;
import org.gabs.hyperdiving.impl.campaign.ids.Gauntlet;

import java.util.ArrayList;
import java.util.List;

public class Dive_RemnantSystem extends Dive_GenericTheme{


    public Dive_RemnantSystem() {
        super();
    }

    //TODO based on difficulty scaling 'n + difficulty' etc
    public StarSystemAPI setTheme(StarSystemAPI system) {
        this.system = system;

        data = BaseThemeGenerator.computeSystemData(this.system);
        subtheme = Dive_ThemeEnum.Subtheme.RESURGENT;

        switch (random.nextInt(gauntletHandler.getCurrentDifficulty()+1)+1){
            case 0:
                subtheme = Dive_ThemeEnum.Subtheme.DESTROYED;
                break;
            case 1:
                subtheme = Dive_ThemeEnum.Subtheme.SUPPRESSED;
                break;
        }

        data.system.addTag(subtheme.getTag());
        data.system.addTag(Tags.THEME_REMNANT);
        data.system.addTag(Tags.THEME_UNSAFE);
        data.system.addTag(Tags.THEME_REMNANT_MAIN);

        addEnemies();

        return this.system;
    }

    public void addEnemies(){
        RemnantSeededFleetManager fleets = null;
        RemnantStationFleetManager activeFleets = null;
        List<CampaignFleetAPI> stations = null;

        int maxFleets = Gauntlet.REMNANT_MIN_FLEETS + random.nextInt(gauntletHandler.getCurrentDifficulty());

        if (subtheme == Dive_ThemeEnum.Subtheme.RESURGENT) {

            fleets = new RemnantSeededFleetManager(
                    data.system,
                    Gauntlet.REMNANT_MIN_FLEETS + gauntletHandler.getCurrentDifficulty(),
                    Gauntlet.REMNANT_MIN_FLEETS + 3 * gauntletHandler.getCurrentDifficulty(),
                    3 + gauntletHandler.getCurrentDifficulty(),
                    11 + gauntletHandler.getCurrentDifficulty()
                    , 0.25f);

            data.system.addScript(fleets);

            stations = addBattlestations(
                    data,
                    1f,
                    1,
                    random.nextInt(gauntletHandler.getCurrentDifficulty() + 2)+1,
                    createStringPicker("remnant_station2_Standard", 10f));

            for (CampaignFleetAPI station : stations)
                activeFleets = new RemnantStationFleetManager(station, 1f, 0, maxFleets, 25f, 6, 12);

        }
        else if (subtheme == Dive_ThemeEnum.Subtheme.RESURGENT) {
            stations = addBattlestations(
                    data,
                    1f,
                    1,
                    1,
                    createStringPicker("remnant_station2_Damaged", 10f));
            for (CampaignFleetAPI station : stations)
                activeFleets = new RemnantStationFleetManager(station, 1f, 0, maxFleets, 15f, 8, 24);

        }
        else
            fleets = new RemnantSeededFleetManager(data.system, Gauntlet.REMNANT_MIN_FLEETS, maxFleets, 1, 2, 0.05f);

        if(fleets != null)
            data.system.addScript(fleets);
        if(activeFleets != null)
            data.system.addScript(activeFleets);

    }

    public List<CampaignFleetAPI> addBattlestations(BaseThemeGenerator.StarSystemData data, float chanceToAddAny, int min, int max,
                                                    WeightedRandomPicker<String> stationTypes) {
        List<CampaignFleetAPI> result = new ArrayList<CampaignFleetAPI>();
        if (random.nextFloat() >= chanceToAddAny) return result;

        int num = min + random.nextInt(max - min + 1);
        if (DEBUG) System.out.println("    Adding " + num + " battlestations");
        for (int i = 0; i < num; i++) {

            BaseThemeGenerator.EntityLocation loc = pickCommonLocation(random, data.system, 200f, true, null);

            String type = stationTypes.pick();
            if (loc != null) {

                CampaignFleetAPI fleet = FleetFactoryV3.createEmptyFleet(Factions.REMNANTS, FleetTypes.BATTLESTATION, null);

                FleetMemberAPI member = Global.getFactory().createFleetMember(FleetMemberType.SHIP, type);
                fleet.getFleetData().addFleetMember(member);

                fleet.getMemoryWithoutUpdate().set(MemFlags.MEMORY_KEY_MAKE_AGGRESSIVE, true);
                fleet.getMemoryWithoutUpdate().set(MemFlags.MEMORY_KEY_NO_JUMP, true);
                fleet.getMemoryWithoutUpdate().set(MemFlags.MEMORY_KEY_MAKE_ALLOW_DISENGAGE, true);
                fleet.addTag(Tags.NEUTRINO_HIGH);

                fleet.setStationMode(true);

                addRemnantStationInteractionConfig(fleet);

                data.system.addEntity(fleet);

                fleet.clearAbilities();
                fleet.addAbility(Abilities.TRANSPONDER);
                fleet.getAbility(Abilities.TRANSPONDER).activate();
                fleet.getDetectedRangeMod().modifyFlat("gen", 1000f);

                fleet.setAI(null);

                setEntityLocation(fleet, loc, null);
                convertOrbitWithSpin(fleet, 5f);

                boolean damaged = type.toLowerCase().contains("damaged");
                String coreId = Commodities.ALPHA_CORE;
                if (damaged) {
                    fleet.getMemoryWithoutUpdate().set("$damagedStation", true);
                    fleet.setName(fleet.getName() + " (Damaged)");
                }

                AICoreOfficerPlugin plugin = Misc.getAICoreOfficerPlugin(coreId);
                PersonAPI commander = plugin.createPerson(coreId, fleet.getFaction().getId(), random);

                fleet.setCommander(commander);
                fleet.getFlagship().setCaptain(commander);

                if (!damaged) {
                    RemnantOfficerGeneratorPlugin.integrateAndAdaptCoreForAIFleet(fleet.getFlagship());
                    RemnantOfficerGeneratorPlugin.addCommanderSkills(commander, fleet, null, 3, random);
                }

                member.getRepairTracker().setCR(member.getRepairTracker().getMaxCR());
                result.add(fleet);

            }
        }

        return result;
    }

    public static void addRemnantStationInteractionConfig(CampaignFleetAPI fleet) {
        fleet.getMemoryWithoutUpdate().set(MemFlags.FLEET_INTERACTION_DIALOG_CONFIG_OVERRIDE_GEN,
                new Dive_RemnantStationInteractionConfig());
    }

    public static class Dive_RemnantStationInteractionConfig implements FleetInteractionDialogPluginImpl.FIDConfigGen {
        public FleetInteractionDialogPluginImpl.FIDConfig createConfig() {
            FleetInteractionDialogPluginImpl.FIDConfig config = new FleetInteractionDialogPluginImpl.FIDConfig();

            config.alwaysAttackVsAttack = true;
            config.leaveAlwaysAvailable = true;
            config.showFleetAttitude = false;
            config.showTransponderStatus = false;
            config.showEngageText = false;


            config.delegate = new FleetInteractionDialogPluginImpl.BaseFIDDelegate() {
                public void postPlayerSalvageGeneration(InteractionDialogAPI dialog, FleetEncounterContext context, CargoAPI salvage) {
                    new RemnantSeededFleetManager.RemnantFleetInteractionConfigGen().createConfig().delegate.
                            postPlayerSalvageGeneration(dialog, context, salvage);
                }
                public void notifyLeave(InteractionDialogAPI dialog) {
                }
                public void battleContextCreated(InteractionDialogAPI dialog, BattleCreationContext bcc) {
                    bcc.aiRetreatAllowed = false;
                    bcc.objectivesAllowed = false;
                }
            };
            return config;
        }
    }

    @Override
    public int getOrder() {
        return 1500;
    }

    @Override
    public String getThemeId() {
        return null;
    }

    @Override
    public void generateForSector(ThemeGenContext themeGenContext, float v) {

    }
}
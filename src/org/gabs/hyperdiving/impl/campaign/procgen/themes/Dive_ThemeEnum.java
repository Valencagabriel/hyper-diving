package org.gabs.hyperdiving.impl.campaign.procgen.themes;

public enum Dive_ThemeEnum {
        JOURNEY,
        BOUNTY,
        GRAVEYARD,
        MEMENTO,
        REMNANT,
        OVERDRIVEN,
        MIDBOSS,
        BOSS;

        public enum Subtheme {
                SCAVENGER("theme_scavenger", "$subtypeScavenger"),
                MINING("theme_mining", "$subtypeMining"),
                RESEARCH("theme_research", "$subtypeResearch"),
                DESTROYED("theme_remnant_destroyed", "$remnantDestroyed"),
                SUPPRESSED("theme_remnant_suppressed", "$remnantSuppressed"),
                RESURGENT("theme_remnant_resurgent", "$remnantResurgent");

                private String tag;
                private String beaconFlag;

                Subtheme(String tag, String beaconFlag) {
                        this.tag = tag;
                        this.beaconFlag = beaconFlag;
                }

                public String getTag() {
                        return this.tag;
                }

                public String getBeaconFlag() {
                        return this.beaconFlag;
                }
        }
}

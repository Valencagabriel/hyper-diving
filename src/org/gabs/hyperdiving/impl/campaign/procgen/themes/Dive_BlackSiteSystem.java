package org.gabs.hyperdiving.impl.campaign.procgen.themes;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.*;
import com.fs.starfarer.api.characters.FullName;
import com.fs.starfarer.api.characters.PersonAPI;
import com.fs.starfarer.api.combat.BattleCreationContext;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import com.fs.starfarer.api.impl.MusicPlayerPluginImpl;
import com.fs.starfarer.api.impl.campaign.*;
import com.fs.starfarer.api.impl.campaign.fleets.FleetFactoryV3;
import com.fs.starfarer.api.impl.campaign.ids.*;
import com.fs.starfarer.api.impl.campaign.procgen.themes.BaseThemeGenerator;
import com.fs.starfarer.api.impl.campaign.procgen.themes.RemnantSeededFleetManager;
import com.fs.starfarer.api.impl.campaign.rulecmd.salvage.special.ShipRecoverySpecial;
import com.fs.starfarer.api.impl.campaign.terrain.MagneticFieldTerrainPlugin;
import com.fs.starfarer.api.impl.campaign.world.ZigLeashAssignmentAI;
import com.fs.starfarer.api.loading.VariantSource;
import com.fs.starfarer.api.util.Misc;
import org.lwjgl.util.vector.Vector2f;

import java.awt.*;

public class Dive_BlackSiteSystem extends Dive_GenericTheme {

    PlanetAPI betaSite = null;

    public Dive_BlackSiteSystem() {
        super();
    }

    public StarSystemAPI setTheme(StarSystemAPI system) {

        this.system = system;
        data = BaseThemeGenerator.computeSystemData(this.system);

        this.system.setLightColor(new Color(225,170,255,255));

        themeUtil.destroyPlanets(system);
        addSite();
        themeUtil.removeStar(system, false);
        addLoot();

        addEnemies();

        data.system.addTag(Tags.THEME_REMNANT);
        data.system.addTag(Tags.THEME_UNSAFE);
        data.system.addTag(Tags.THEME_REMNANT_MAIN);

        return this.system;
    }

    public void addEnemies(){
        CampaignFleetAPI fleet = FleetFactoryV3.createEmptyFleet(Factions.NEUTRAL, FleetTypes.PATROL_LARGE, null);
        fleet.setName("Mirage");
        fleet.setNoFactionInName(true);
        fleet.getMemoryWithoutUpdate().set(MemFlags.MEMORY_KEY_MAKE_HOSTILE, true);
        fleet.getMemoryWithoutUpdate().set(MemFlags.MEMORY_KEY_MAKE_AGGRESSIVE, true);
        fleet.getMemoryWithoutUpdate().set(MemFlags.MEMORY_KEY_NO_REP_IMPACT, true);
        fleet.getMemoryWithoutUpdate().set(MemFlags.MEMORY_KEY_LOW_REP_IMPACT, true);
        fleet.getMemoryWithoutUpdate().set(MemFlags.MEMORY_KEY_NO_SHIP_RECOVERY, true);
        fleet.getMemoryWithoutUpdate().set(MemFlags.MEMORY_KEY_MAKE_ALWAYS_PURSUE, true);
        fleet.getMemoryWithoutUpdate().set("$ziggurat", true);

        fleet.getMemoryWithoutUpdate().set(MusicPlayerPluginImpl.KEEP_PLAYING_LOCATION_MUSIC_DURING_ENCOUNTER_MEM_KEY, true);

        fleet.getFleetData().addFleetMember("ziggurat_Experimental");
        fleet.getFleetData().ensureHasFlagship();

        fleet.clearAbilities();
        fleet.setTransponderOn(true);

        PersonAPI person = createZigguratCaptain();
        fleet.setCommander(person);

        FleetMemberAPI flagship = fleet.getFlagship();
        flagship.setCaptain(person);
        flagship.updateStats();
        flagship.getRepairTracker().setCR(flagship.getRepairTracker().getMaxCR());
        flagship.setShipName("DIV Mirage");

        flagship.setVariant(flagship.getVariant().clone(), false, false);
        flagship.getVariant().setSource(VariantSource.REFIT);
        flagship.getVariant().addTag(Tags.SHIP_LIMITED_TOOLTIP);


        Vector2f loc = new Vector2f(betaSite.getLocation().x + 300 * ((float) Math.random() - 0.5f),
                betaSite.getLocation().y + 300 * ((float) Math.random() - 0.5f));
        fleet.setLocation(loc.x, loc.y);

        betaSite.getContainingLocation().addEntity(fleet);

        fleet.getMemoryWithoutUpdate().set(MemFlags.FLEET_INTERACTION_DIALOG_CONFIG_OVERRIDE_GEN,
                new ZigFIDConfig());

        //TODO generate own leash ai
        fleet.addScript(new ZigLeashAssignmentAI(fleet, betaSite));
    }

    public static PersonAPI createZigguratCaptain() {
        PersonAPI person = Global.getFactory().createPerson();
        person.setName(new FullName("Motes", "", FullName.Gender.ANY));
        person.setFaction(Factions.NEUTRAL);
        person.setPortraitSprite(Global.getSettings().getSpriteName("characters", "ziggurat_captain"));
        person.setPersonality(Personalities.RECKLESS);
        person.setRankId(Ranks.SPACE_CAPTAIN);
        person.setPostId(null);

        person.getStats().setSkipRefresh(true);

        person.getStats().setLevel(10);
        person.getStats().setSkillLevel(Skills.HELMSMANSHIP, 2);
        person.getStats().setSkillLevel(Skills.TARGET_ANALYSIS, 2);
        person.getStats().setSkillLevel(Skills.IMPACT_MITIGATION, 2);
        person.getStats().setSkillLevel(Skills.GUNNERY_IMPLANTS, 2);
        person.getStats().setSkillLevel(Skills.ENERGY_WEAPON_MASTERY, 2);
        person.getStats().setSkillLevel(Skills.COMBAT_ENDURANCE, 2);
        //person.getStats().setSkillLevel(Skills.RELIABILITY_ENGINEERING, 2);
        //person.getStats().setSkillLevel(Skills.RANGED_SPECIALIZATION, 2);
        person.getStats().setSkillLevel(Skills.POLARIZED_ARMOR, 2);
        person.getStats().setSkillLevel(Skills.MISSILE_SPECIALIZATION, 2);
        //person.getStats().setSkillLevel(Skills.PHASE_MASTERY, 2);
        person.getStats().setSkillLevel(Skills.FIELD_MODULATION, 2);
        person.getStats().setSkillLevel(Skills.DAMAGE_CONTROL, 2);

        person.getStats().setSkillLevel(Skills.NAVIGATION, 1);

        person.getStats().setSkipRefresh(false);

        return person;
    }

    public void addLoot(){
        SectorEntityToken cache = BaseThemeGenerator.addSalvageEntity(system, Entities.ALPHA_SITE_WEAPONS_CACHE, Factions.NEUTRAL);
        cache.getMemoryWithoutUpdate().set("$ttWeaponsCache", true);
        cache.getLocation().set(11111, 11111);
    }

    public void addSite(){

        betaSite = system.addPlanet("site_beta", this.system.getStar(), "Beta Site","irradiated", 0, 150, 1200, 40);

        betaSite.getMarket().addCondition(Conditions.NO_ATMOSPHERE);
        betaSite.getMarket().addCondition(Conditions.COLD);
        betaSite.getMarket().addCondition(Conditions.DARK);
        betaSite.getMarket().addCondition(Conditions.IRRADIATED);
        betaSite.getMarket().addCondition(Conditions.RUINS_SCATTERED);

        CoreLifecyclePluginImpl.addRuinsJunk(betaSite);

        betaSite.setOrbit(null);
        betaSite.setLocation(0, 0);

        SectorEntityToken field = system.addTerrain(Terrain.MAGNETIC_FIELD,
                new MagneticFieldTerrainPlugin.MagneticFieldParams(150f, // terrain effect band width
                        500, // terrain effect middle radius
                        betaSite, // entity that it's around
                        350f, // visual band start
                        650f, // visual band end
                        new Color(60, 60, 150, 90), // base color
                        1f, // probability to spawn aurora sequence, checked once/day when no aurora in progress
                        new Color(130, 60, 150, 130),
                        new Color(150, 30, 120, 150),
                        new Color(200, 50, 130, 190),
                        new Color(250, 70, 150, 240),
                        new Color(200, 80, 130, 255),
                        new Color(75, 0, 160, 255),
                        new Color(127, 0, 255, 255)
                ));
        field.setCircularOrbit(betaSite, 0, 0, 75);

        CustomCampaignEntityAPI beacon = system.addCustomEntity(null, null, Entities.WARNING_BEACON, Factions.NEUTRAL);
        beacon.setCircularOrbitPointingDown(betaSite, 0, 2500, 60);

        beacon.getMemoryWithoutUpdate().set(WarningBeaconEntityPlugin.PING_ID_KEY, Pings.WARNING_BEACON3);
        beacon.getMemoryWithoutUpdate().set(WarningBeaconEntityPlugin.PING_FREQ_KEY, 1.5f);
        beacon.getMemoryWithoutUpdate().set(WarningBeaconEntityPlugin.PING_COLOR_KEY, new Color(250,125,0,255));
        beacon.getMemoryWithoutUpdate().set(WarningBeaconEntityPlugin.GLOW_COLOR_KEY, new Color(250,55,0,255));
    }

    public static class ZigFIDConfig implements FleetInteractionDialogPluginImpl.FIDConfigGen {
        public FleetInteractionDialogPluginImpl.FIDConfig createConfig() {
            FleetInteractionDialogPluginImpl.FIDConfig config = new FleetInteractionDialogPluginImpl.FIDConfig();

            config.showTransponderStatus = false;
            config.showEngageText = false;
            config.alwaysPursue = true;
            config.dismissOnLeave = false;
            config.withSalvage = false;
            config.printXPToDialog = true;

            config.noSalvageLeaveOptionText = "Continue";

            config.delegate = new FleetInteractionDialogPluginImpl.BaseFIDDelegate() {
                public void postPlayerSalvageGeneration(InteractionDialogAPI dialog, FleetEncounterContext context, CargoAPI salvage) {
                    new RemnantSeededFleetManager.RemnantFleetInteractionConfigGen().createConfig().delegate.
                            postPlayerSalvageGeneration(dialog, context, salvage);
                }
                public void notifyLeave(InteractionDialogAPI dialog) {

                    SectorEntityToken other = dialog.getInteractionTarget();
                    if (!(other instanceof CampaignFleetAPI)) {
                        dialog.dismiss();
                        return;
                    }
                    CampaignFleetAPI fleet = (CampaignFleetAPI) other;

                    if (!fleet.isEmpty()) {
                        dialog.dismiss();
                        return;
                    }

                    ShipRecoverySpecial.PerShipData ship = new ShipRecoverySpecial.PerShipData("ziggurat_Hull", ShipRecoverySpecial.ShipCondition.WRECKED, 0f);
                    ship.shipName = "DIV Mirage";
                    DerelictShipEntityPlugin.DerelictShipData params = new DerelictShipEntityPlugin.DerelictShipData(ship, false);
                    CustomCampaignEntityAPI entity = (CustomCampaignEntityAPI) BaseThemeGenerator.addSalvageEntity(
                            fleet.getContainingLocation(),
                            Entities.WRECK, Factions.NEUTRAL, params);
                    Misc.makeImportant(entity, "ziggurat");
                    //entity.getMemoryWithoutUpdate().set("$ziggurat", true);

                    entity.getLocation().x = fleet.getLocation().x + (50f - (float) Math.random() * 100f);
                    entity.getLocation().y = fleet.getLocation().y + (50f - (float) Math.random() * 100f);

                    ShipRecoverySpecial.ShipRecoverySpecialData data = new ShipRecoverySpecial.ShipRecoverySpecialData(null);
                    data.notNowOptionExits = true;
                    data.noDescriptionText = true;
                    DerelictShipEntityPlugin dsep = (DerelictShipEntityPlugin) entity.getCustomPlugin();
                    ShipRecoverySpecial.PerShipData copy = (ShipRecoverySpecial.PerShipData) dsep.getData().ship.clone();
                    copy.variant = Global.getSettings().getVariant(copy.variantId).clone();
                    copy.variantId = null;
                    copy.variant.addTag(Tags.SHIP_CAN_NOT_SCUTTLE);
                    copy.variant.addTag(Tags.SHIP_UNIQUE_SIGNATURE);
                    data.addShip(copy);

                    Misc.setSalvageSpecial(entity, data);

                    dialog.setInteractionTarget(entity);
                    RuleBasedInteractionDialogPluginImpl plugin = new RuleBasedInteractionDialogPluginImpl("AfterZigguratDefeat");
                    dialog.setPlugin(plugin);
                    plugin.init(dialog);
                }

                public void battleContextCreated(InteractionDialogAPI dialog, BattleCreationContext bcc) {
                    bcc.aiRetreatAllowed = false;
                    bcc.objectivesAllowed = false;
                    bcc.fightToTheLast = true;
                    bcc.enemyDeployAll = true;
                }
            };
            return config;
        }
    }
}

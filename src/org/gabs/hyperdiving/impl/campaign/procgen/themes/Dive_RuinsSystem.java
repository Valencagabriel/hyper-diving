package org.gabs.hyperdiving.impl.campaign.procgen.themes;

import com.fs.starfarer.api.campaign.CampaignFleetAPI;
import com.fs.starfarer.api.campaign.PlanetAPI;
import com.fs.starfarer.api.campaign.SectorEntityToken;
import com.fs.starfarer.api.campaign.StarSystemAPI;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import com.fs.starfarer.api.impl.campaign.ids.*;
import com.fs.starfarer.api.impl.campaign.procgen.themes.BaseThemeGenerator;
import com.fs.starfarer.api.impl.campaign.procgen.themes.ThemeGenContext;
import org.gabs.hyperdiving.impl.campaign.ids.Gauntlet;

import java.util.ArrayList;
import java.util.List;

public class Dive_RuinsSystem extends Dive_GenericTheme {


    public Dive_RuinsSystem() {
        super();
    }

    public StarSystemAPI setTheme(StarSystemAPI system) {
        this.system = system;

        data = BaseThemeGenerator.computeSystemData(this.system);

        switch (random.nextInt(3)){
            case 0:
                subtheme = Dive_ThemeEnum.Subtheme.SCAVENGER;
                break;
            case 1:
                subtheme = Dive_ThemeEnum.Subtheme.MINING;
                break;
            case 2:
                subtheme = Dive_ThemeEnum.Subtheme.RESEARCH;
                break;
        }

        log.info("Subtheme: " + subtheme);

        data.system.addTag(Tags.THEME_INTERESTING);
        data.system.addTag(Tags.THEME_RUINS);
        data.system.addTag(Tags.THEME_RUINS_MAIN);

        if (gauntletHandler.getCurrentDifficulty() > 1)
            upgradePlanetsRuins();

        switch (random.nextInt(4)){
            case 0:
                addProbes(system,Gauntlet.PROBES_MIN + gauntletHandler.getCurrentDifficulty());
                addCryosleeper(system);
                log.info("special: cryosleeper");
                break;
            case 1:
                addProbes(system, Gauntlet.PROBES_MIN + gauntletHandler.getCurrentDifficulty());
                addMothership(system);
                log.info("special: mothership");
                break;
            default:
                addProbes(system,random.nextInt(3) + Gauntlet.PROBES_MIN + gauntletHandler.getCurrentDifficulty());
                overrideEnemyLocation = true;
                overrideGuide = true;

                switch (this.subtheme){
                    case SCAVENGER:
                        guide = themeUtil.addDockyardToSystem(system,true,null);
                        guide.getMemoryWithoutUpdate().set("$dive_dockyard_guide",true);
                        log.info("special: dock");
                        break;
                    case MINING:
                        guide = themeUtil.addEntityToSystem(system,Entities.STATION_MINING_REMNANT).entity;
                        log.info("special: mining station");
                        break;
                    case RESEARCH:
                        guide = themeUtil.addEntityToSystem(system,Entities.STATION_RESEARCH_REMNANT).entity;
                        log.info("special: research station");
                        break;
                }

                themeUtil.overrideGuideGeneration(guide,this.subtheme);
                break;
        }

        addEnemies();

        return this.system;
    }

    public void addEnemies(){
        if(gauntletHandler.getCurrentDifficulty() > 1 || random.nextInt(gauntletHandler.getCurrentDifficulty()+1) > 0){
            CampaignFleetAPI fleet = null;
            SectorEntityToken beacon;

            for (int i = 0; i < gauntletHandler.getCurrentDifficulty(); i++){
                switch (subtheme){
                    case MINING:
                        fleet = fleetUtil.createDerelictFleet();
                        fleet = fleetUtil.addOverdriveToFleet(fleet);
                        fleet.setName("Rampaging miners");
                        break;
                    case RESEARCH:
                        fleet = fleetUtil.createResearchersFleet();
                        fleet = fleetUtil.addAutomatedToFleet(fleet);
                        fleet.setName("Unknown researchers");
                        break;
                    case SCAVENGER:
                        fleet = fleetUtil.createScavengerFleet();
                        fleet = fleetUtil.addAutomatedToFleet(fleet);
                        fleet.setName("Lost scavengers");
                        break;
                    default:
                        fleet = fleetUtil.createScavengerFleet();
                        fleet = fleetUtil.addAutomatedToFleet(fleet);
                        fleet.setName("Lost fleet");
                        break;
                }

                fleet.setNoFactionInName(true);

                fleet.getFleetData().ensureHasFlagship();
                fleet = fleetUtil.addAutomatedCaptainsToFleet(fleet);
                fleet = fleetUtil.setDefaultFleetMemory(fleet);

                for (FleetMemberAPI curr : fleet.getFleetData().getMembersListCopy()) {
                    fleetUtil.makeAICoreSkillsGoodForLowTech(curr,true);
                    curr.getRepairTracker().setCR(curr.getRepairTracker().getMaxCR());
                }

                if (!overrideEnemyLocation)
                    beacon = this.system.getPlanets().get(random.nextInt(system.getPlanets().size()));
                else {
                    beacon = guide;
                    //TODO generate own leash ai
                    //fleet.addScript(new Dive_LeashAI(fleet, beacon));
                }

                if (beacon != null)
                    beacon.getContainingLocation().addEntity(fleet);
                else
                    fleet.setLocation(6666,6666);

                fleet.setCommander(fleet.getFlagship().getCaptain());
            }
        }
    }

    public void upgradePlanetsRuins() {
        for (PlanetAPI planet : system.getPlanets()) {
            if (planet.isStar()) continue;
            if (planet.isGasGiant()) continue;
            if (planet.getMarket() == null) continue;
            if (!planet.getMarket().isPlanetConditionMarketOnly()) continue;

            if (planet.getMarket().hasCondition(Conditions.RUINS_SCATTERED)) {
                planet.getMarket().removeCondition(Conditions.RUINS_SCATTERED);
                planet.getMarket().addCondition(Conditions.RUINS_WIDESPREAD);
            }
            else if (planet.getMarket().hasCondition(Conditions.RUINS_WIDESPREAD)) {
                planet.getMarket().removeCondition(Conditions.RUINS_WIDESPREAD);
                planet.getMarket().addCondition(Conditions.RUINS_EXTENSIVE);
            }
            else if (planet.getMarket().hasCondition(Conditions.RUINS_EXTENSIVE)) {
                planet.getMarket().removeCondition(Conditions.RUINS_EXTENSIVE);
                planet.getMarket().addCondition(Conditions.RUINS_VAST);
            }
            else if (!planet.getMarket().hasCondition(Conditions.RUINS_VAST))
                if (random.nextInt(2) > 0)
                    planet.getMarket().addCondition(Conditions.RUINS_SCATTERED);

        }
    }

    protected AddedEntity addCryosleeper(StarSystemAPI system) {
        AddedEntity entity = themeUtil.addEntityToSystem(system,Entities.DERELICT_CRYOSLEEPER);
        system.addTag(Tags.THEME_DERELICT_CRYOSLEEPER);
        return entity;
    }

    protected AddedEntity addMothership(StarSystemAPI system) {
        AddedEntity entity = themeUtil.addEntityToSystem(system,Entities.DERELICT_MOTHERSHIP);
        system.addTag(Tags.THEME_DERELICT_MOTHERSHIP);
        return entity;
    }

    protected List<AddedEntity> addProbes(StarSystemAPI system, int num) {
        List<AddedEntity> result = new ArrayList<AddedEntity>();
        for (int i = 0; i < num; i++) {
            AddedEntity probe = themeUtil.addEntityToSystem(system,Entities.DERELICT_SURVEY_PROBE);
            if (probe != null)
                result.add(probe);
        }
        return result;
    }

    //etc
    @Override
    public int getOrder() {
        return 0;
    }

    @Override
    public String getThemeId() {
        return null;
    }

    @Override
    public void generateForSector(ThemeGenContext themeGenContext, float v) {

    }
}

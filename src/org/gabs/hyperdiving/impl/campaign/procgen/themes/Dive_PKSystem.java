package org.gabs.hyperdiving.impl.campaign.procgen.themes;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.*;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import com.fs.starfarer.api.impl.campaign.DerelictShipEntityPlugin;
import com.fs.starfarer.api.impl.campaign.fleets.DefaultFleetInflater;
import com.fs.starfarer.api.impl.campaign.fleets.DefaultFleetInflaterParams;
import com.fs.starfarer.api.impl.campaign.fleets.FleetFactoryV3;
import com.fs.starfarer.api.impl.campaign.fleets.FleetParamsV3;
import com.fs.starfarer.api.impl.campaign.ids.*;
import com.fs.starfarer.api.impl.campaign.procgen.DefenderDataOverride;
import com.fs.starfarer.api.impl.campaign.procgen.PlanetConditionGenerator;
import com.fs.starfarer.api.impl.campaign.procgen.themes.BaseThemeGenerator;
import com.fs.starfarer.api.impl.campaign.procgen.themes.PKDefenderPluginImpl;
import com.fs.starfarer.api.impl.campaign.procgen.themes.SalvageSpecialAssigner;
import com.fs.starfarer.api.impl.campaign.rulecmd.salvage.SalvageGenFromSeed;
import com.fs.starfarer.api.impl.campaign.rulecmd.salvage.special.ShipRecoverySpecial;
import com.fs.starfarer.api.impl.campaign.terrain.AsteroidFieldTerrainPlugin;
import com.fs.starfarer.api.util.Misc;
import com.fs.starfarer.api.util.WeightedRandomPicker;

import java.util.List;
import java.util.Random;

import static com.fs.starfarer.api.impl.campaign.procgen.themes.MiscellaneousThemeGenerator.getRemnantStations;


public class Dive_PKSystem extends Dive_GenericTheme{

    public Dive_PKSystem() {
        super();
    }

    public StarSystemAPI setTheme(StarSystemAPI system) {

        system.addTag(Tags.THEME_SPECIAL);
        system.addTag(Tags.PK_SYSTEM);

        //Global.getSector().getPersistentData().put(PK_SYSTEM_KEY, system);
        //Global.getSector().getMemoryWithoutUpdate().set(PK_SYSTEM_KEY, system.getId());


        PlanetAPI tundra = null;
        for (PlanetAPI curr : system.getPlanets()) {
            if (curr.isStar()) continue;
            //if (curr.isMoon()) continue;
            if (curr.isGasGiant()) continue;
            if (curr.getMarket() == null) continue;
            if (!curr.getMarket().isPlanetConditionMarketOnly()) continue;
            if (curr.getCircularOrbitRadius() < 6000) continue;

            tundra = curr;
            break;
        }

        if (tundra == null) {
            List<BaseThemeGenerator.OrbitGap> gaps = BaseThemeGenerator.findGaps(system.getCenter(), 6000, 20000, 800);
            float orbitRadius = 7000;
            if (!gaps.isEmpty()) {
                orbitRadius = (gaps.get(0).start + gaps.get(0).end) * 0.5f;
            }
            float orbitDays = orbitRadius / (20f + random.nextFloat() * 5f);
            float radius = 100f + random.nextFloat() * 50f;
            float angle = random.nextFloat() * 360f;
            String type = Planets.BARREN;
            String name = "Sentinel";
            tundra = system.addPlanet(Misc.genUID(), system.getStar(), name, type, angle, radius, orbitRadius, orbitDays);

            if (tundra == null) {
                log.info("FAILED TO CREATE PLANET IN PK SYSTEM");
                return null;
            }
        }

        tundra.setName("Sentinel");
        tundra.addTag(Tags.NOT_RANDOM_MISSION_TARGET);
        //tundra.getMemoryWithoutUpdate().set(PK_PLANET_KEY, true);
        //Global.getSector().getPersistentData().put(PK_PLANET_KEY, tundra);

        log.info("Setting planet [" + tundra.getName() + "] to tundra");
        tundra.changeType(Planets.TUNDRA, random);
        tundra.getMarket().getConditions().clear();
        PlanetConditionGenerator.generateConditionsForPlanet(null, tundra, system.getAge());
        tundra.getMarket().removeCondition(Conditions.DECIVILIZED);
        tundra.getMarket().removeCondition(Conditions.DECIVILIZED_SUBPOP);
        tundra.getMarket().removeCondition(Conditions.RUINS_EXTENSIVE);
        tundra.getMarket().removeCondition(Conditions.RUINS_SCATTERED);
        tundra.getMarket().removeCondition(Conditions.RUINS_VAST);
        tundra.getMarket().removeCondition(Conditions.RUINS_WIDESPREAD);
        tundra.getMarket().removeCondition(Conditions.INIMICAL_BIOSPHERE);

        tundra.getMarket().removeCondition(Conditions.FARMLAND_POOR);
        tundra.getMarket().removeCondition(Conditions.FARMLAND_ADEQUATE);
        tundra.getMarket().removeCondition(Conditions.FARMLAND_RICH);
        tundra.getMarket().removeCondition(Conditions.FARMLAND_BOUNTIFUL);
        tundra.getMarket().addCondition(Conditions.FARMLAND_POOR);


        for (SectorEntityToken curr : system.getEntitiesWithTag(Tags.STABLE_LOCATION)) {
            system.removeEntity(curr);
        }
        for (SectorEntityToken curr : system.getEntitiesWithTag(Tags.OBJECTIVE)) {
            system.removeEntity(curr);
        }


        List<BaseThemeGenerator.OrbitGap> gaps = BaseThemeGenerator.findGaps(system.getCenter(), 2000, 20000, 800);
        float orbitRadius = 7000;
        if (!gaps.isEmpty()) {
            orbitRadius = (gaps.get(0).start + gaps.get(0).end) * 0.5f;
        }
        float radius = 500f + 200f * random.nextFloat();
        float area = radius * radius * 3.14f;
        int count = (int) (area / 80000f);
        count *= 2;
        if (count < 10) count = 10;
        if (count > 100) count = 100;
        float angle = random.nextFloat() * 360f;
        float orbitDays = orbitRadius / (20f + random.nextFloat() * 5f);

        SectorEntityToken field = system.addTerrain(Terrain.ASTEROID_FIELD,
                new AsteroidFieldTerrainPlugin.AsteroidFieldParams(
                        radius, // min radius
                        radius + 100f, // max radius
                        count, // min asteroid count
                        count, // max asteroid count
                        4f, // min asteroid radius
                        16f, // max asteroid radius
                        null)); // null for default name

        field.setCircularOrbit(system.getCenter(), angle, orbitRadius, orbitDays);

        pointOfInterest = BaseThemeGenerator.addSalvageEntity(system, Entities.STATION_RESEARCH_REMNANT, Factions.NEUTRAL);
        //cache.getMemoryWithoutUpdate().set(PK_CACHE_KEY, true);
        pointOfInterest.addTag(Tags.NOT_RANDOM_MISSION_TARGET);
        pointOfInterest.setCircularOrbit(field, 0, 0, 100f);

        //TODO use difficulty settings here
        Misc.setDefenderOverride(pointOfInterest, new DefenderDataOverride(Factions.HEGEMONY, 1f, 20, 20, 1));
        addEnemies();

        BaseThemeGenerator.StarSystemData data = new BaseThemeGenerator.StarSystemData();
        WeightedRandomPicker<String> derelictShipFactions = new WeightedRandomPicker<String>(random);
        derelictShipFactions.add(Factions.LUDDIC_PATH);
        WeightedRandomPicker<String> hulls = new WeightedRandomPicker<String>(random);
        hulls.add("prometheus2", 1f);
        hulls.add("colossus2", 1f);
        hulls.add("colossus2", 1f);
        hulls.add("colossus2", 1f);
        hulls.add("eradicator", 1f);
        hulls.add("enforcer", 1f);
        hulls.add("sunder", 1f);
        hulls.add("venture_pather", 1f);
        hulls.add("manticore_luddic_path", 1f);
        hulls.add("cerberus_luddic_path", 1f);
        hulls.add("hound_luddic_path", 1f);
        hulls.add("buffalo2", 1f);
        themeUtil.addShipGraveyard(data, field, derelictShipFactions, hulls);
        for (BaseThemeGenerator.AddedEntity ae : data.generated) {
            SalvageSpecialAssigner.assignSpecials(ae.entity, true);
        }

        float max = 0f;
        JumpPointAPI fringePoint = null;
        List<JumpPointAPI> points = system.getEntities(JumpPointAPI.class);
        for (JumpPointAPI curr : points) {
            float dist = curr.getCircularOrbitRadius();
            if (dist > max) {
                max = dist;
                fringePoint = curr;
            }
        }

        if (fringePoint != null) {
            data = new BaseThemeGenerator.StarSystemData();
            WeightedRandomPicker<String> remnantShipFactions = new WeightedRandomPicker<String>(random);
            remnantShipFactions.add(Factions.REMNANTS);
            hulls = new WeightedRandomPicker<String>(random);
            hulls.add("radiant", 0.25f);
            hulls.add("nova", 0.5f);
            hulls.add("brilliant", 1f);
            hulls.add("apex", 1f);
            hulls.add("scintilla", 1f);
            hulls.add("scintilla", 1f);
            hulls.add("fulgent", 1f);
            hulls.add("fulgent", 1f);
            hulls.add("glimmer", 1f);
            hulls.add("glimmer", 1f);
            hulls.add("lumen", 1f);
            hulls.add("lumen", 1f);
            themeUtil.addShipGraveyard(data, fringePoint, remnantShipFactions, hulls);
            themeUtil.addDebrisField(data, fringePoint, 400f);

            for (BaseThemeGenerator.AddedEntity ae : data.generated) {
                SalvageSpecialAssigner.assignSpecials(ae.entity, true);
                if (ae.entity.getCustomPlugin() instanceof DerelictShipEntityPlugin) {
                    DerelictShipEntityPlugin plugin = (DerelictShipEntityPlugin) ae.entity.getCustomPlugin();
                    plugin.getData().ship.condition = ShipRecoverySpecial.ShipCondition.WRECKED;
                }
            }
        }

        // Improvised dockyard where presumably the ship conversion took place
        SectorEntityToken dockyard = system.addCustomEntity("pk_dockyard",
                "Sentinel Gantries", Entities.ORBITAL_DOCKYARD, "neutral");

        dockyard.setCircularOrbitPointingDown(tundra, 45, 300, 30);
        dockyard.setCustomDescriptionId("pk_orbital_dockyard");
        dockyard.getMemoryWithoutUpdate().set("$pkDockyard", true);

        //neutralStation.setInteractionImage("illustrations", "abandoned_station2");
        Misc.setAbandonedStationMarket("pk_dockyard", dockyard);


        // add some unused stuff to the dockyard
        CargoAPI cargo = dockyard.getMarket().getSubmarket(Submarkets.SUBMARKET_STORAGE).getCargo();
        cargo.initMothballedShips(Factions.HEGEMONY);

        CampaignFleetAPI temp = Global.getFactory().createEmptyFleet(Factions.HEGEMONY, null, true);
        temp.getFleetData().addFleetMember("enforcer_XIV_Elite");
        temp.getFleetData().addFleetMember("enforcer_XIV_Elite");
        temp.getFleetData().addFleetMember("eagle_xiv_Elite");
        temp.getFleetData().addFleetMember("dominator_XIV_Elite");
        DefaultFleetInflaterParams p = new DefaultFleetInflaterParams();
        p.quality = -1;
        temp.setInflater(new DefaultFleetInflater(p));
        temp.inflateIfNeeded();
        temp.setInflater(null);

        int index = 0;
        for (FleetMemberAPI member : temp.getFleetData().getMembersListCopy()) {
            for (String slotId : member.getVariant().getFittedWeaponSlots()) {
                String weaponId = member.getVariant().getWeaponId(slotId);
                if (random.nextFloat() < 0.5f) {
                    member.getVariant().clearSlot(slotId);
                }
                if (random.nextFloat() < 0.25f) {
                    cargo.addWeapons(weaponId, 1);
                }
            }
            if (index == 0 || index == 2) {
                cargo.getMothballedShips().addFleetMember(member);
            }
            index++;
        }
        cargo.addCommodity(Commodities.METALS, 50f + random.nextInt(51));

        List<CampaignFleetAPI> stations = getRemnantStations(true, false);
        float minDist = Float.MAX_VALUE;
        CampaignFleetAPI nexus = null;
        for (CampaignFleetAPI curr : stations) {
            float dist = Misc.getDistanceLY(tundra, curr);
            if (dist < minDist) {
                minDist = dist;
                nexus = curr;
            }
        }
        //if (nexus != null) {
        //log.info("Found Remnant nexus in [" + nexus.getContainingLocation().getName() + "]");
        //nexus.getMemoryWithoutUpdate().set(PK_NEXUS_KEY, true);
        //Global.getSector().getPersistentData().put(PK_NEXUS_KEY, nexus);
        //Global.getSector().getMemoryWithoutUpdate().set(PK_NEXUS_KEY, nexus.getContainingLocation().getId());

        //Misc.addDefeatTrigger(nexus, "PKNexusDefeated");
        //}
        return system;
    }


    //TODO make my own defender plugin
    public void addEnemies(){

        SalvageGenFromSeed.SDMParams p = new SalvageGenFromSeed.SDMParams();
        p.entity = pointOfInterest;
        p.factionId = Factions.HEGEMONY;

        FleetParamsV3 fParams = new FleetParamsV3(null, null,
                Factions.HEGEMONY,
                0f,
                FleetTypes.PATROL_SMALL,
                20,
                0, 0, 0, 0, 0, 0);


        Long seed = random.nextLong();
        Random fleetRandom = Misc.getRandom(seed, 1);
        CampaignFleetAPI defenders = FleetFactoryV3.createFleet(fParams);

        PKDefenderPluginImpl pkDefenderPlugin = new PKDefenderPluginImpl();
        pkDefenderPlugin.modifyFleet(p, defenders, fleetRandom, true);
        pointOfInterest.getMemory().set("$hasDefenders", true);
        pointOfInterest.getMemory().set("$defenderFleet", defenders);
    }
}

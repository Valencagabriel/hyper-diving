package org.gabs.hyperdiving.impl.campaign.procgen.themes;

import com.fs.starfarer.api.campaign.CampaignFleetAPI;
import com.fs.starfarer.api.campaign.PlanetAPI;
import com.fs.starfarer.api.campaign.StarSystemAPI;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import com.fs.starfarer.api.impl.campaign.fleets.FleetFactoryV3;
import com.fs.starfarer.api.impl.campaign.ids.Factions;
import com.fs.starfarer.api.impl.campaign.ids.FleetTypes;
import com.fs.starfarer.api.impl.campaign.ids.MemFlags;
import com.fs.starfarer.api.impl.campaign.ids.Tags;
import com.fs.starfarer.api.impl.campaign.procgen.themes.BaseThemeGenerator;
import com.fs.starfarer.api.impl.campaign.procgen.themes.OmegaOfficerGeneratorPlugin;
import com.fs.starfarer.api.impl.campaign.procgen.themes.ThemeGenContext;
import com.fs.starfarer.api.impl.campaign.world.ZigLeashAssignmentAI;
import com.fs.starfarer.api.util.Misc;
import org.lwjgl.util.vector.Vector2f;

import java.awt.*;


public class Dive_OmegaSystem extends Dive_GenericTheme {

    OmegaOfficerGeneratorPlugin officerPlugin;

    //TODO revisit later
    //TODO based on difficulty scaling 'n + difficulty' etc
    //TODO add a true boss
    public StarSystemAPI setTheme(StarSystemAPI system) {

        this.system = system;
        data = BaseThemeGenerator.computeSystemData(this.system);

        this.system.setLightColor(new Color(225,170,255,255));

        officerPlugin = new OmegaOfficerGeneratorPlugin();

        addEnemies();

        data.system.addTag(Tags.THEME_REMNANT);
        data.system.addTag(Tags.THEME_UNSAFE);
        data.system.addTag(Tags.THEME_REMNANT_MAIN);
        data.system.addTag(Tags.OMEGA);

        return this.system;
    }

    @Override
    public void addEnemies() {
        for(PlanetAPI planet: this.system.getPlanets()){
            if (!planet.isStar())
                addFleet(planet);
        }
    }

    public void addFleet(PlanetAPI planet){
        CampaignFleetAPI fleet = FleetFactoryV3.createEmptyFleet(Factions.OMEGA, FleetTypes.PATROL_LARGE, null);

        fleet.setName("Shimmering Entities");
        fleet.setNoFactionInName(true);
        fleet.knowsWhoPlayerIs();

        fleet = fleetUtil.setDefaultFleetMemory(fleet);

        fleet.getFleetData().addFleetMember(fleetUtil.largeOmegaPicker().pick());

        fleet.getFleetData().addFleetMember(fleetUtil.mediumOmegaPicker().pick());
        fleet.getFleetData().addFleetMember(fleetUtil.mediumOmegaPicker().pick());
        fleet.getFleetData().addFleetMember(fleetUtil.mediumOmegaPicker().pick());

        fleet.getFleetData().ensureHasFlagship();

        fleet.clearAbilities();
        fleet.setTransponderOn(true);

        officerPlugin.addCommanderAndOfficers(fleet, null, Misc.random);

        for(FleetMemberAPI member : fleet.getFleetData().getMembersInPriorityOrder()){
            member.updateStats();
            member.getRepairTracker().setCR(member.getRepairTracker().getMaxCR());
        }

        Vector2f loc = new Vector2f(planet.getLocation().x + 300 * ((float) Math.random() - 0.5f),
                planet.getLocation().y + 300 * ((float) Math.random() - 0.5f));
        fleet.setLocation(loc.x, loc.y);

        planet.getContainingLocation().addEntity(fleet);
        fleet.addScript(new ZigLeashAssignmentAI(fleet, planet));

    }

    @Override
    public int getOrder() {
        return 0;
    }

    @Override
    public String getThemeId() {
        return null;
    }

    @Override
    public void generateForSector(ThemeGenContext themeGenContext, float v) {

    }
}

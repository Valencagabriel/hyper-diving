package org.gabs.hyperdiving.impl.campaign.procgen.themes;

import com.fs.starfarer.api.campaign.PlanetAPI;
import com.fs.starfarer.api.campaign.SectorEntityToken;
import com.fs.starfarer.api.campaign.StarSystemAPI;
import com.fs.starfarer.api.impl.campaign.DerelictShipEntityPlugin;
import com.fs.starfarer.api.impl.campaign.ids.Entities;
import com.fs.starfarer.api.impl.campaign.ids.Factions;
import com.fs.starfarer.api.impl.campaign.ids.Tags;
import com.fs.starfarer.api.impl.campaign.ids.Terrain;
import com.fs.starfarer.api.impl.campaign.procgen.themes.BaseThemeGenerator;
import com.fs.starfarer.api.impl.campaign.procgen.themes.ThemeGenContext;
import com.fs.starfarer.api.impl.campaign.rulecmd.salvage.special.ShipRecoverySpecial;
import com.fs.starfarer.api.impl.campaign.terrain.AsteroidFieldTerrainPlugin;
import com.fs.starfarer.api.impl.campaign.terrain.StarCoronaTerrainPlugin;
import com.fs.starfarer.api.util.Misc;
import com.fs.starfarer.api.util.WeightedRandomPicker;
import org.gabs.hyperdiving.Dive_HyperDiving;
import org.gabs.hyperdiving.impl.campaign.Dive_GauntletHandler;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Random;

public class Dive_ThemeUtil extends BaseThemeGenerator{

    protected Random random;
    Dive_GauntletHandler gauntletHandler;

    float radius = 0;
    float area = 0;
    int count = 0;
    float angle = 0;
    float orbitDays = 0;
    float orbitRadius = 0;

    public Dive_ThemeUtil() {
        random = new Random();
        gauntletHandler = Dive_HyperDiving.getGauntletHandler();
    }


    //THEME SETTING

    public StarSystemAPI setBossTheme(StarSystemAPI system) {
        return new Dive_OmegaSystem().setTheme(system);
    }

    public StarSystemAPI setRemnantTheme(StarSystemAPI system) {
        return new Dive_RemnantSystem().setTheme(system);
    }

    public StarSystemAPI setRuinsTheme(StarSystemAPI system) {
        return new Dive_RuinsSystem().setTheme(system);
    }

    public StarSystemAPI setPlanetKillerTheme(StarSystemAPI system) {
        return new Dive_PKSystem().setTheme(system);
    }

    public StarSystemAPI setBlackSiteTheme(StarSystemAPI system) {
        return new Dive_BlackSiteSystem().setTheme(system);
    }

    public StarSystemAPI setMementoTheme(StarSystemAPI system) {
        switch (random.nextInt(2)){
            case 0:
                return setPlanetKillerTheme(system);
            case 1:
                return setBlackSiteTheme(system);
        }
        return setPlanetKillerTheme(system);
    }

    //POINTS OF INTEREST
    public void overrideGuideGeneration(SectorEntityToken guide, Dive_ThemeEnum.Subtheme subtheme){
        gauntletHandler.getStarSystemHandler().setOverrideGuide(true);
        gauntletHandler.getStarSystemHandler().setGuide(guide);
        gauntletHandler.getStarSystemHandler().setCurrentSubtheme(subtheme);
    }

    public void addShipGraveyard(BaseThemeGenerator.StarSystemData data, float chanceToAddAny, int min, int max, WeightedRandomPicker<String> factions) {
        if (random.nextFloat() >= chanceToAddAny) return;
        int num = min + random.nextInt(max - min + 1);

        for (int i = 0; i < num; i++) {
            LinkedHashMap<BaseThemeGenerator.LocationType, Float> weights = new LinkedHashMap<BaseThemeGenerator.LocationType, Float>();

            weights.put(BaseThemeGenerator.LocationType.STAR_ORBIT, 5f);
            WeightedRandomPicker<BaseThemeGenerator.EntityLocation> locs = getLocations(random, data.system, null, 1000f, weights);
            BaseThemeGenerator.EntityLocation loc = locs.pick();

            if (loc != null) {
                SectorEntityToken token = data.system.createToken(0, 0);
                data.system.addEntity(token);
                setEntityLocation(token, loc, null);
                addShipGraveyard(data, token, factions);
            }
        }

    }

    public SectorEntityToken addDerelict(StarSystemAPI system, SectorEntityToken focus, String variantId,ShipRecoverySpecial.ShipCondition condition, float orbitRadius, boolean recoverable) {
        DerelictShipEntityPlugin.DerelictShipData params = new DerelictShipEntityPlugin.DerelictShipData(new ShipRecoverySpecial.PerShipData(variantId, condition, 0f), false);
        SectorEntityToken ship = BaseThemeGenerator.addSalvageEntity(system, Entities.WRECK, Factions.NEUTRAL, params);
        ship.setDiscoverable(true);

        orbitDays = orbitRadius / (10f + (float) Math.random() * 5f);
        ship.setCircularOrbit(focus, (float) Math.random() * 360f, orbitRadius, orbitDays);
        return ship;
    }

    public WeightedRandomPicker<EntityLocation> locationPicker(StarSystemAPI system){
        LinkedHashMap<LocationType, Float> weights = new LinkedHashMap<LocationType, Float>();
        weights.put(LocationType.PLANET_ORBIT, 10f);
        weights.put(LocationType.JUMP_ORBIT, 1f);
        weights.put(LocationType.NEAR_STAR, 1f);
        //weights.put(LocationType.OUTER_SYSTEM, 5f);
        weights.put(LocationType.IN_ASTEROID_BELT, 10f);
        weights.put(LocationType.IN_RING, 10f);
        weights.put(LocationType.IN_ASTEROID_FIELD, 10f);
        weights.put(LocationType.STAR_ORBIT, 1f);
        weights.put(LocationType.IN_SMALL_NEBULA, 1f);
        weights.put(LocationType.L_POINT, 1f);
        WeightedRandomPicker<EntityLocation> locs = getLocations(random, system, 100f, weights);
        return locs;
    }

    public SectorEntityToken addDockyardToSystem(StarSystemAPI system, boolean inRandomPosition,SectorEntityToken toOrbit){
        //TODO create my own
        SectorEntityToken dockyard = system.addCustomEntity("dive_dockyard",
                "Lost gantry", Entities.ORBITAL_DOCKYARD, "neutral");
        Misc.setAbandonedStationMarket("dive_dockyard", dockyard);
        dockyard.getMemoryWithoutUpdate().set("$dive_dockyard", true);

        if (inRandomPosition){
            SectorEntityToken asteroidField = addAsteroidField(system);
            dockyard.setCircularOrbit(asteroidField, 0, 0, 0);
        }
        else
            dockyard.setCircularOrbitPointingDown(toOrbit, 45, 300, 30);


        return dockyard;
    }

    public AddedEntity addEntityToSystem(StarSystemAPI system,String entityId){
        WeightedRandomPicker<EntityLocation> locs = locationPicker(system);
        AddedEntity entity = addEntity(random, system, locs, entityId, Factions.DERELICT);

        return entity;
    }
    //SYSTEM MODIFICATIONS
    public SectorEntityToken addAsteroidField(StarSystemAPI system){
        List<BaseThemeGenerator.OrbitGap> gaps = findGaps(system.getCenter(), 2000, 20000, 800);
        orbitRadius = 7000;
        if (!gaps.isEmpty()) {
            orbitRadius = (gaps.get(0).start + gaps.get(0).end) * 0.5f;
        }

        radius = 500f + 200f * random.nextFloat();
        area = radius * radius * 3.14f;
        count = (int) (area / 80000f);
        count *= 2;
        if (count < 10) count = 10;
        if (count > 100) count = 100;
        angle = random.nextFloat() * 360f;
        orbitDays = orbitRadius / (20f + random.nextFloat() * 5f);

        SectorEntityToken field = system.addTerrain(Terrain.ASTEROID_FIELD,
                new AsteroidFieldTerrainPlugin.AsteroidFieldParams(
                        radius, // min radius
                        radius + 100f, // max radius
                        count, // min asteroid count
                        count, // max asteroid count
                        4f, // min asteroid radius
                        16f, // max asteroid radius
                        null)); // null for default name

        field.setCircularOrbit(system.getCenter(), angle, orbitRadius, orbitDays);
        return field;
    }

    public void removeStar(StarSystemAPI system, boolean removeOrbits){
        StarCoronaTerrainPlugin coronaPlugin = Misc.getCoronaFor(system.getStar());
        if (coronaPlugin != null) {
            system.removeEntity(coronaPlugin.getEntity());
        }
        system.initNonStarCenter();

        if(removeOrbits){
            for (SectorEntityToken entity : system.getAllEntities()) {
                if (entity.getOrbitFocus() == system.getStar() ||
                        entity.getOrbitFocus() == system.getCenter()) {
                    entity.setOrbit(null);
                }
            }
        }

        system.removeEntity(system.getStar());
        system.setStar(null);
        system.getCenter().addTag(Tags.AMBIENT_LS);
    }

    public void destroyPlanets(StarSystemAPI system) {
        List<PlanetAPI> toRemove = new ArrayList<>();
        for(PlanetAPI planet :system.getPlanets()){
            if(planet.isStar())
                continue;

            radius = planet.getRadius();
            area = radius * radius * 3.14f;
            count = (int) (area / 80000f);

            SectorEntityToken field = system.addTerrain(Terrain.ASTEROID_FIELD,
                    new AsteroidFieldTerrainPlugin.AsteroidFieldParams(
                            radius, // min radius
                            radius + 100f, // max radius
                            count, // min asteroid count
                            count, // max asteroid count
                            4f, // min asteroid radius
                            16f, // max asteroid radius
                            null)); // null for default name

            field.setCircularOrbit(system.getCenter(), planet.getCircularOrbitAngle(), planet.getCircularOrbitRadius(), planet.getCircularOrbitPeriod());
            toRemove.add(planet);
        }
        for(PlanetAPI planet :toRemove){
            system.removeEntity(planet);
        }
    }

    //ETC

    @Override
    public int getOrder() {
        return 0;
    }

    @Override
    public String getThemeId() {
        return null;
    }

    @Override
    public void generateForSector(ThemeGenContext themeGenContext, float v) {
    }


}

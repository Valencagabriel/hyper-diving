package org.gabs.hyperdiving.impl.campaign.procgen.themes;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.SectorEntityToken;
import com.fs.starfarer.api.campaign.StarSystemAPI;
import com.fs.starfarer.api.impl.campaign.procgen.themes.BaseThemeGenerator;
import com.fs.starfarer.api.impl.campaign.procgen.themes.ThemeGenContext;
import org.apache.log4j.Logger;
import org.gabs.hyperdiving.Dive_HyperDiving;
import org.gabs.hyperdiving.impl.campaign.Dive_GauntletHandler;
import org.gabs.hyperdiving.impl.campaign.procgen.Dive_FleetUtil;

import java.util.Random;

public abstract class Dive_GenericTheme extends BaseThemeGenerator {

    public static Logger log = Global.getLogger(Dive_GenericTheme.class);
    protected static Random random = new Random();

    Dive_ThemeUtil themeUtil;
    Dive_GauntletHandler gauntletHandler;
    Dive_FleetUtil fleetUtil;

    StarSystemAPI system;
    BaseThemeGenerator.StarSystemData data;

    Dive_ThemeEnum.Subtheme subtheme;

    Boolean overrideEnemyLocation = false;
    SectorEntityToken pointOfInterest = null;

    Boolean overrideGuide = false;
    SectorEntityToken guide = null;


    public Dive_GenericTheme() {
        gauntletHandler = Dive_HyperDiving.getGauntletHandler();
        themeUtil = Dive_HyperDiving.getThemeUtil();
        fleetUtil = new Dive_FleetUtil();
    }

    public abstract StarSystemAPI setTheme(StarSystemAPI system);

    public abstract void addEnemies();

    //etc
    @Override
    public int getOrder() {
        return 0;
    }

    @Override
    public String getThemeId() {
        return null;
    }

    @Override
    public void generateForSector(ThemeGenContext themeGenContext, float v) {

    }

    public SectorEntityToken getPointOfInterest() {
        return pointOfInterest;
    }

    public Boolean getOverrideGuide() {
        return overrideGuide;
    }

}

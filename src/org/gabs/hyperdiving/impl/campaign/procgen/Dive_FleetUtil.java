package org.gabs.hyperdiving.impl.campaign.procgen;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.AICoreOfficerPlugin;
import com.fs.starfarer.api.campaign.CampaignFleetAPI;
import com.fs.starfarer.api.campaign.FactionAPI;
import com.fs.starfarer.api.characters.PersonAPI;
import com.fs.starfarer.api.combat.ShipVariantAPI;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import com.fs.starfarer.api.impl.campaign.fleets.FleetFactoryV3;
import com.fs.starfarer.api.impl.campaign.fleets.FleetParamsV3;
import com.fs.starfarer.api.impl.campaign.ids.*;
import com.fs.starfarer.api.loading.VariantSource;
import com.fs.starfarer.api.util.Misc;
import com.fs.starfarer.api.util.WeightedRandomPicker;
import org.gabs.hyperdiving.Dive_HyperDiving;
import org.gabs.hyperdiving.impl.campaign.Dive_GauntletHandler;

import java.util.Collection;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

public class Dive_FleetUtil {

    protected Random random;
    Dive_GauntletHandler gauntletHandler;

    WeightedRandomPicker<String> largeOmega;
    WeightedRandomPicker<String> mediumOmega;
    WeightedRandomPicker<String> smallBountyShip;

    //TODO maybe create a generic fleet picker
    public Dive_FleetUtil() {
        random = new Random();
        gauntletHandler = Dive_HyperDiving.getGauntletHandler();

        largeOmega = new WeightedRandomPicker(Misc.random);
        mediumOmega = new WeightedRandomPicker(Misc.random);
        smallBountyShip = new WeightedRandomPicker(Misc.random);

        largeOmega.add("tesseract_Attack");
        largeOmega.add("tesseract_Attack2");
        largeOmega.add("tesseract_Strike");
        largeOmega.add("tesseract_Disruptor");

        mediumOmega.add("facet_Attack");
        mediumOmega.add("facet_Attack2");
        mediumOmega.add("facet_Armorbreaker");
        mediumOmega.add("facet_Shieldbreaker");
        mediumOmega.add("facet_Defense");
        mediumOmega.add("facet_Missile");
    }

    //FLEET CREATOR
    public CampaignFleetAPI createRandomFleet(String faction) {
        if (random == null) random = new Random();

        WeightedRandomPicker<String> picker = new WeightedRandomPicker<String>(random);
        if (gauntletHandler.getCurrentDifficulty()<2)
            picker.add(FleetTypes.SCAVENGER_SMALL, 5f);
        if (gauntletHandler.getCurrentDifficulty()<3)
            picker.add(FleetTypes.SCAVENGER_MEDIUM, 15f + gauntletHandler.getCurrentDifficulty()*3);
        if (gauntletHandler.getCurrentDifficulty()>2)
            picker.add(FleetTypes.MERC_ARMADA, 10f + gauntletHandler.getCurrentDifficulty()*5);

        picker.add(FleetTypes.SCAVENGER_LARGE, 10f + gauntletHandler.getCurrentDifficulty()*5);

        String type = picker.pick();

        int combat = 0;
        int freighter = 0;
        int tanker = 0;
        int transport = 0;
        int utility = 0;

        if (type.equals(FleetTypes.SCAVENGER_SMALL)) {
            combat = random.nextInt(3) + 1 + gauntletHandler.getCurrentDifficulty();
            tanker = random.nextInt(2) + 1;
            utility = random.nextInt(2) + 1;
        } else if (type.equals(FleetTypes.SCAVENGER_MEDIUM)) {
            combat = 5 + random.nextInt(5) + gauntletHandler.getCurrentDifficulty();
            freighter = 2 + random.nextInt(2);
            tanker = 1 + random.nextInt(2);
            transport = random.nextInt(2);
            utility = 1 + random.nextInt(2);
        } else if (type.equals(FleetTypes.SCAVENGER_LARGE)) {
            combat = 8 + random.nextInt(8) + gauntletHandler.getCurrentDifficulty();
            freighter = 2 + random.nextInt(2);
            tanker = 2 + random.nextInt(2);
            transport =  random.nextInt(2);
            utility = 2 + random.nextInt(3);
        } else if (type.equals(FleetTypes.MERC_ARMADA)) {
            combat = 9 + random.nextInt(10) + gauntletHandler.getCurrentDifficulty();
            freighter = 1 + random.nextInt(2);
            tanker = 1 + random.nextInt(2);
            transport =  random.nextInt(2);
            utility = 1 + random.nextInt(3);
        }

        combat *= 5f  + gauntletHandler.getCurrentDifficulty();
        freighter *= 2f;
        tanker *= 2f;
        transport *= 1.5f;

        FleetParamsV3 params = new FleetParamsV3(
                null,
                null,
                faction, // quality will always be reduced by non-market-faction penalty, which is what we want
                null,
                type,
                combat, // combatPts
                freighter, // freighterPts
                tanker, // tankerPts
                transport, // transportPts
                0f, // linerPts
                utility, // utilityPts
                0f // qualityMod
        );

        params.random = random;
        CampaignFleetAPI fleet = FleetFactoryV3.createFleet(params);

        if (fleet == null || fleet.isEmpty()) return null;

        fleet.setFaction(Factions.REMNANTS, true);

        return fleet;
    }

    public CampaignFleetAPI createScavengerFleet() {
        return createRandomFleet(Factions.SCAVENGERS);
    }

    public CampaignFleetAPI createResearchersFleet() {
        return createRandomFleet(Factions.TRITACHYON);
    }

    public CampaignFleetAPI createDerelictFleet() {
        return createRandomFleet(Factions.DERELICT);
    }

    //FLEET EDITOR
    public CampaignFleetAPI addOverdriveToFleet(CampaignFleetAPI fleet){
        ShipVariantAPI variant = null;
        for (FleetMemberAPI ship : fleet.getFleetData().getMembersListCopy()) {

            variant = ship.getVariant().clone();
            variant.setSource(VariantSource.REFIT);
            variant.addTag(Tags.TAG_NO_AUTOFIT);

            variant.addPermaMod(HullMods.SAFETYOVERRIDES);
            variant.setVariantDisplayName("Overdriven");
            variant.addTag(Tags.TAG_AUTOMATED_NO_PENALTY);
            variant.addTag(Tags.AUTOMATED_RECOVERABLE);

            ship.setVariant(variant, false, false);
        }
        return fleet;
    }

    public CampaignFleetAPI addAutomatedToFleet(CampaignFleetAPI fleet){
        ShipVariantAPI variant = null;
        for (FleetMemberAPI ship : fleet.getFleetData().getMembersListCopy()) {

            variant = ship.getVariant().clone();
            variant.setSource(VariantSource.REFIT);
            variant.addTag(Tags.TAG_NO_AUTOFIT);

            variant.addPermaMod(HullMods.AUTOMATED);
            variant.setVariantDisplayName("Automated");
            variant.addTag(Tags.TAG_AUTOMATED_NO_PENALTY);
            variant.addTag(Tags.AUTOMATED_RECOVERABLE);

            ship.setVariant(variant, false, false);

        }
        return fleet;
    }

    public CampaignFleetAPI addAutomatedCaptainsToFleet(CampaignFleetAPI fleet){
        for (FleetMemberAPI ship : fleet.getFleetData().getMembersListCopy()) {
            if (gauntletHandler.getCurrentDifficulty()>5 && ship.isFlagship())
                addAutomatedCaptain(ship, fleet.getFaction().getId(), Commodities.OMEGA_CORE, random);
            else if(ship.isFlagship() || gauntletHandler.getCurrentDifficulty()>5)
                addAutomatedCaptain(ship, fleet.getFaction().getId(), Commodities.ALPHA_CORE, random);
            else if(random.nextInt(2 + gauntletHandler.getCurrentDifficulty())>0)
                addAutomatedCaptain(ship, fleet.getFaction().getId(), Commodities.BETA_CORE, random);
            else
                addAutomatedCaptain(ship, fleet.getFaction().getId(), Commodities.GAMMA_CORE, random);
        }
        return fleet;
    }

    public static void addAutomatedCaptain(FleetMemberAPI member, String factionId, String aiCore, Random random) {
        AICoreOfficerPlugin plugin = Misc.getAICoreOfficerPlugin(Commodities.ALPHA_CORE);
        member.setId("diveAuto_" + random.nextLong());

        if (aiCore != null) {
            PersonAPI person = plugin.createPerson(aiCore, factionId, random);
            member.setCaptain(person);
        }
    }

    public static void makeAICoreSkillsGoodForLowTech(FleetMemberAPI member, boolean integrate) {
        if (member.getCaptain() == null || !member.getCaptain().isAICore()) return;

        PersonAPI person = member.getCaptain();
        person.getStats().setSkipRefresh(true);

        if (integrate) {
            person.getStats().setLevel(person.getStats().getLevel() + 1);
            person.getStats().setSkillLevel(Skills.BALLISTIC_MASTERY, 2);
        }

        if (member.isCapital() || member.isCruiser()) {
            person.getStats().setSkillLevel(Skills.COMBAT_ENDURANCE, 0);
            person.getStats().setSkillLevel(Skills.MISSILE_SPECIALIZATION, 2);
        }

        person.getStats().setSkipRefresh(false);
    }

    public CampaignFleetAPI setDefaultFleetMemory(CampaignFleetAPI fleet){
        fleet.getMemoryWithoutUpdate().set(MemFlags.MEMORY_KEY_MAKE_HOSTILE, true);
        fleet.getMemoryWithoutUpdate().set(MemFlags.MEMORY_KEY_MAKE_AGGRESSIVE, true);
        fleet.getMemoryWithoutUpdate().set(MemFlags.MEMORY_KEY_NO_REP_IMPACT, true);
        fleet.getMemoryWithoutUpdate().set(MemFlags.MEMORY_KEY_LOW_REP_IMPACT, true);
        fleet.getMemoryWithoutUpdate().set(MemFlags.MEMORY_KEY_MAKE_ALWAYS_PURSUE, true);
        fleet.getMemoryWithoutUpdate().set(MemFlags.FLEET_FIGHT_TO_THE_LAST, true);

        return fleet;
    }

    //SHIP PICKER
    public WeightedRandomPicker<String> largeOmegaPicker(){
        return largeOmega;
    }

    public WeightedRandomPicker<String> mediumOmegaPicker(){
        return mediumOmega;
    }

    public WeightedRandomPicker<String> smallBountyShipPicker(){
        return pickVariant(Factions.MERCENARY, random, "combatSmall", 7.0F, "combatFreighterSmall", 3.0F);
    }

    public WeightedRandomPicker<String> smallJourneyShipPicker() {
        return pickVariant(Factions.MERCENARY, random,  "freighterSmall", 10.0F, "linerSmall", 10.0F, "tankerSmall", 10.0F, "personnelSmall", 5.0F);
    }

    public WeightedRandomPicker<String> pickSmallVariantId(String factionId) {
        return pickVariant(factionId, random, "combatSmall", 10.0F, "combatFreighterSmall", 3.0F, "freighterSmall", 1.0F, "tankerSmall", 1.0F, "linerSmall", 1.0F, "personnelSmall", 1.0F);
    }

    public WeightedRandomPicker<String> pickMediumVariantId(String factionId) {
        return pickVariant(factionId, random, "combatMedium", 10.0F, "combatFreighterMedium", 3.0F, "carrierSmall", 1.0F, "freighterMedium", 1.0F, "tankerMedium", 1.0F, "linerMedium", 1.0F, "personnelMedium", 1.0F);
    }

    public WeightedRandomPicker<String> pickLargeVariantId(String factionId) {
        return pickVariant(factionId, random, "combatLarge", 10.0F, "combatCapital", 3.0F, "combatFreighterLarge", 1.0F, "carrierMedium", 1.0F, "freighterLarge", 1.0F, "carrierLarge", 1.0F, "tankerLarge", 1.0F, "linerLarge", 1.0F, "tankerMedium", 1.0F, "personnelLarge", 1.0F);
    }

    public static WeightedRandomPicker<String> pickVariant(String factionId, Random random, Object... shipRoles) {
        if (random == null) {
            random = new Random();
        }

        FactionAPI faction = Global.getSector().getFaction(factionId);
        WeightedRandomPicker<String> picker = new WeightedRandomPicker(random);

        String role;
        for(int i = 0; i < shipRoles.length; i += 2) {
            role = (String)shipRoles[i];
            Float weight = (Float)shipRoles[i + 1];
            picker.add(role, weight);
        }

        Object variantsForRole;
        for(variantsForRole = new HashSet(); ((Set)variantsForRole).isEmpty() && !picker.isEmpty(); variantsForRole = faction.getVariantsForRole(role)) {
            role = (String)picker.pickAndRemove();
            if (role == null) {
                return null;
            }
        }

        picker.clear();
        picker.addAll((Collection)variantsForRole);
        return picker;
    }
}

package org.gabs.hyperdiving.impl.campaign.procgen;

import com.fs.starfarer.api.EveryFrameScript;
import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.*;
import com.fs.starfarer.api.impl.campaign.ids.Entities;
import com.fs.starfarer.api.impl.campaign.ids.Factions;
import com.fs.starfarer.api.impl.campaign.ids.Tags;
import com.fs.starfarer.api.impl.campaign.procgen.Constellation;
import com.fs.starfarer.api.impl.campaign.procgen.StarSystemGenerator;
import com.fs.starfarer.api.impl.campaign.procgen.themes.BaseThemeGenerator;
import com.fs.starfarer.api.impl.campaign.rulecmd.salvage.special.ShipRecoverySpecial;
import com.fs.starfarer.api.impl.campaign.terrain.StarCoronaTerrainPlugin;
import com.fs.starfarer.api.util.Misc;
import com.fs.starfarer.api.util.WeightedRandomPicker;
import org.apache.log4j.Logger;
import org.gabs.hyperdiving.Dive_HyperDiving;

import org.gabs.hyperdiving.impl.campaign.ids.Gauntlet;
import org.gabs.hyperdiving.impl.campaign.procgen.themes.Dive_ThemeUtil;
import org.gabs.hyperdiving.impl.campaign.procgen.themes.Dive_ThemeEnum;
import org.gabs.hyperdiving.utilities.IntToRoman;
import org.lwjgl.util.vector.Vector2f;
import sound.F;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Random;

import static org.gabs.hyperdiving.utilities.IntToRoman.intToRoman;

public class Dive_StarSystemHandler extends StarSystemGenerator {

    public static Logger log = Global.getLogger(Dive_StarSystemHandler.class);
    protected static Random random = new Random();

    Dive_ThemeUtil themeUtil;
    Dive_FleetUtil fleetUtil;

    Constellation.ConstellationType type;
    Constellation constellation = null;

    protected int currentDepth;
    protected SectorEntityToken originGate;
    protected SectorEntityToken gauntletGate;

    Boolean overrideGuide = false;
    protected SectorEntityToken guide;
    protected SectorEntityToken beacon;

    protected Dive_ThemeEnum currentTheme;
    protected Dive_ThemeEnum.Subtheme currentSubtheme;
    private boolean newGauntlet;

    public Dive_StarSystemHandler(CustomConstellationParams params, int currentDepth, SectorEntityToken originGate) {
        super(params);
        this.currentDepth = currentDepth;
        this.originGate = originGate;

        themeUtil = Dive_HyperDiving.getThemeUtil();
        fleetUtil = new Dive_FleetUtil();
        random = new Random();
    }

    @Override
    protected boolean initSystem(String name, Vector2f loc) {
        sector = Global.getSector();

        if (newGauntlet){
            if(Global.getSector().getStarSystem(Gauntlet.NEW_SYSTEM) != null)
                system = Global.getSector().getStarSystem(Gauntlet.NEW_SYSTEM);
            else if(Global.getSector().getStarSystem(Gauntlet.OLD_SYSTEM) != null)
                system = Global.getSector().getStarSystem(Gauntlet.OLD_SYSTEM);
            else if(Global.getSector().getStarSystem(Gauntlet.OVERRIDE_SYSTEM) != null)
                system = Global.getSector().getStarSystem(Gauntlet.OVERRIDE_SYSTEM);
            else
                system = sector.createStarSystem(name);
        }
        else{
            if(Global.getSector().getStarSystem(Gauntlet.OVERRIDE_SYSTEM) != null)
                system = Global.getSector().getStarSystem(Gauntlet.OVERRIDE_SYSTEM);
            else
                system = sector.createStarSystem(name);
        }
        system.setBaseName(name);

        cleanSystem();

        system.setProcgen(true);
        system.setType(systemType);
        system.getLocation().set(loc);
        hyper = Global.getSector().getHyperspace();

        system.setBackgroundTextureFilename(backgroundName);
        return true;
    }

    public void cleanSystem(){
        List<SectorEntityToken> entityList = new ArrayList<>(system.getAllEntities());
        for (SectorEntityToken entity : entityList) {
            system.removeEntity(entity);
        }
        List<FleetStubAPI> fleetStubAPIS = new ArrayList<>(system.getFleetStubs());
        for (FleetStubAPI entity : fleetStubAPIS) {
            system.removeFleetStub(entity);
        }
        List<EveryFrameScript> everyFrameScripts = new ArrayList<>(system.getScripts());
        for (EveryFrameScript entity : everyFrameScripts) {
            system.removeScript(entity);
        }
        system.clearTags();
        system.getMemoryWithoutUpdate().clear();
        system.setEnteredByPlayer(false);
        system.setDoNotShowIntelFromThisLocationOnMap(true);

    }

    @Override
    public void generateSystem(Vector2f loc) {

        systemType = pickSystemType(constellationAge);

        String uuid = Misc.genUID();

        String id = "system_" + uuid;
        String name = Gauntlet.NEW_SYSTEM;

        if (!initSystem(name, loc)) {
            cleanup();
            return;
        }

        star = null;
        secondary = null;
        tertiary = null;
        systemCenter = null;

        if (!addStars(id)) {
            cleanup();
            return;
        }

        updateAgeAfterPickingStar();

        float binaryPad = 1500f;

        GenResult result = addPlanetsAndTerrain(MAX_ORBIT_RADIUS);

        float primaryOrbitalRadius = star.getRadius();
        if (result != null) {
            primaryOrbitalRadius = result.orbitalWidth * 0.5f;
        }

        // add far stars, if needed
        float orbitAngle = random.nextFloat() * 360f;
        float baseOrbitRadius = primaryOrbitalRadius + binaryPad;
        float orbitDays = baseOrbitRadius / (3f + random.nextFloat() * 2f);
        if (systemType == StarSystemType.BINARY_FAR && secondary != null) {
            addFarStar(secondary, orbitAngle, baseOrbitRadius, orbitDays);
        } else if (systemType == StarSystemType.TRINARY_1CLOSE_1FAR && tertiary != null) {
            addFarStar(tertiary, orbitAngle, baseOrbitRadius, orbitDays);
        } else if (systemType == StarSystemType.TRINARY_2FAR) {
            addFarStar(secondary, orbitAngle, baseOrbitRadius, orbitDays);
            addFarStar(tertiary, orbitAngle + 60f + 180f * random.nextFloat(), baseOrbitRadius, orbitDays);
        }


        if (systemType == StarSystemType.NEBULA) {
            star.setSkipForJumpPointAutoGen(true);
        }

        addJumpPoints(result, false);

        if (systemType == StarSystemType.NEBULA) {
            system.removeEntity(star);
            StarCoronaTerrainPlugin coronaPlugin = Misc.getCoronaFor(star);
            if (coronaPlugin != null) {
                system.removeEntity(coronaPlugin.getEntity());
            }
            system.setStar(null);
            system.initNonStarCenter();
            for (SectorEntityToken entity : system.getAllEntities()) {
                if (entity.getOrbitFocus() == star ||
                        entity.getOrbitFocus() == system.getCenter()) {
                    entity.setOrbit(null);
                }
            }
            system.getCenter().addTag(Tags.AMBIENT_LS);
        }

        //this is what was changed from the original. commented so we dont need to clean it up
        //system.autogenerateHyperspaceJumpPoints(true, false);

        if (systemType == StarSystemType.NEBULA)
            system.setStar(star);

        system.setName("Gauntlet - Depth " + IntToRoman.intToRoman(currentDepth));
        addStableLocations();
        addSystemwideNebula();
        system.generateAnchorIfNeeded();

    }

    public StarSystemAPI handleSystemGeneration(Vector2f location,boolean newGauntlet) {

        this.newGauntlet = newGauntlet;
        resetVariables();
        decideSystemTheme();

        pickNebulaAndBackground();

        generateSystem(location);
        addTagsToSystem();
        handleConstellation();

        nameSystemEntities();
        addThemeToSystem();
        addGuideToSystem();
        handleGauntletGates();

        handleJumpPoints();

        return this.system;
    }

    protected void resetVariables(){
        this.system = null;
        this.overrideGuide = false;
        this.guide = null;
    }

    protected void addTagsToSystem(){
        this.system.addTag(Tags.NOT_RANDOM_MISSION_TARGET);
        this.system.addTag(Tags.SYSTEM_CUT_OFF_FROM_HYPER);
        this.system.addTag(Tags.THEME_SPECIAL);
    }

    protected void handleGauntletGates(){
        List<SectorEntityToken> gates = this.system.getEntitiesWithTag("gate");

        if (gates.isEmpty())
            this.gauntletGate = addGate(this.system);
        else
            this.gauntletGate = gates.get(0);
        this.gauntletGate.addTag("dive_gauntletGate");
        this.gauntletGate.getMemoryWithoutUpdate().set("$gateScanned",true);
    }

    protected SectorEntityToken addGate(StarSystemAPI gauntletSystem){
        LinkedHashMap<BaseThemeGenerator.LocationType, Float> weights = new LinkedHashMap<BaseThemeGenerator.LocationType, Float>();
        weights.put(BaseThemeGenerator.LocationType.STAR_ORBIT, 10f);
        weights.put(BaseThemeGenerator.LocationType.OUTER_SYSTEM, 10f);
        WeightedRandomPicker<BaseThemeGenerator.EntityLocation> locs;

        try{
            locs = BaseThemeGenerator.getLocations(random, gauntletSystem, null, 100f, weights);
        }
        catch (NullPointerException e){
            log.error("No gaps found");
            gauntletGate = gauntletSystem.addCustomEntity(null, null, Entities.INACTIVE_GATE,  Factions.NEUTRAL);
            gauntletGate.setLocation(6666,6666);
            return gauntletGate;
        }

        BaseThemeGenerator.EntityLocation loc = locs.pick();

        SectorEntityToken gauntletGate;

        if (loc != null && loc.orbit != null) {
            gauntletGate = gauntletSystem.addCustomEntity(null, null, Entities.INACTIVE_GATE,  Factions.NEUTRAL);
            gauntletGate.setOrbit(loc.orbit);
            loc.orbit.setEntity(gauntletGate);
        }
        else if (loc.location != null){
            gauntletGate = gauntletSystem.addCustomEntity(null, null, Entities.INACTIVE_GATE,  Factions.NEUTRAL);
            gauntletGate.setLocation(loc.location.x,loc.location.y);
        }
        else {
            gauntletGate = gauntletSystem.addCustomEntity(null, null, Entities.INACTIVE_GATE,  Factions.NEUTRAL);
            gauntletGate.setLocation(6666,6666);
        }

        return gauntletGate;
    }

    //TODO add random orbit
    //TODO bosses should be hidden fleet with transponder on(see blacksite) and not derelicts
    protected void addGuideToSystem(){
        if (overrideGuide)
            return;

        int starCont = 0;
        beacon = this.system.getStar();
        List<PlanetAPI> planets = this.system.getPlanets();

        for (PlanetAPI planet: planets){
            if (planet.isStar())
                starCont++;
        }

        if (planets.size() > starCont)
            beacon = planets.get(random.nextInt(planets.size()-starCont)+starCont);

        if (beacon == null)
            beacon = systemCenter;

        switch (currentTheme){
            case JOURNEY:
                guide = themeUtil.addDerelict(system, beacon, fleetUtil.smallJourneyShipPicker().pick(), ShipRecoverySpecial.ShipCondition.BATTERED, beacon.getRadius() + 200f, true);
                break;
            case BOUNTY:
            default:
                guide = themeUtil.addDerelict(system, beacon, fleetUtil.smallBountyShipPicker().pick(), ShipRecoverySpecial.ShipCondition.BATTERED, beacon.getRadius() + 200f, true);
                break;
        }

    }

    protected void decideSystemTheme(){
        if (currentDepth == 1)
            this.currentTheme = Dive_ThemeEnum.JOURNEY;
        else{
            if(currentDepth % Gauntlet.BOSS_DEPTH != 0){
                if (currentDepth % Gauntlet.MID_DEPTH != 0){
                    switch (random.nextInt(4)){
                        case 0:
                            this.currentTheme = Dive_ThemeEnum.BOUNTY;
                            break;
                        case 1:
                            this.currentTheme = Dive_ThemeEnum.JOURNEY;
                            break;
                        case 2:
                            this.currentTheme = Dive_ThemeEnum.GRAVEYARD;
                            break;
                        case 3:
                            this.currentTheme = Dive_ThemeEnum.REMNANT;
                            break;
                    }
                }
                else
                    this.currentTheme = Dive_ThemeEnum.MEMENTO;
            }
            else
                this.currentTheme = Dive_ThemeEnum.BOSS;
        }
        log.info("Choosen theme: " + currentTheme);
    }

    boolean debug = false;
    protected void addThemeToSystem(){
        StarSystemAPI tempSystem = null;
        if (debug){
            this.system = themeUtil.setRuinsTheme(this.system);
            return;
        }
        switch (currentTheme){
            case MEMENTO:
                tempSystem = themeUtil.setMementoTheme(this.system);
                break;
            case BOSS:
                tempSystem = themeUtil.setBossTheme(this.system);
                break;
            case REMNANT:
                tempSystem = themeUtil.setRemnantTheme(this.system);
                break;
            case JOURNEY:
            default:
                tempSystem = themeUtil.setRuinsTheme(this.system);
                break;
        }
        this.system = tempSystem;
    }

    //TODO Check if constellations are garbage collected after system deletion
    protected void handleConstellation(){
        if (nebulaType.equals(NEBULA_NONE))
            type = Constellation.ConstellationType.NORMAL;
        else
            type = Constellation.ConstellationType.NEBULA;

        Constellation constellation = new Constellation(type, constellationAge);
        this.system.setConstellation(constellation);
        constellation.getSystems().add(system);
    }

    protected void nameSystemEntities(){
        int planetCont = 1;
        int starCont = 1;
        for(PlanetAPI planet: this.system.getPlanets()){
            if(planet.isStar()){
                if (!currentTheme.equals(Dive_ThemeEnum.MEMENTO))
                    planet.setName("Lost System "+ ((starCont==1)?"":(intToRoman(starCont)+" ")) + "- Depth " + intToRoman(currentDepth));
                else
                    planet.setName("Memento "+ ((starCont>1)?"":(intToRoman(starCont)+" ")) +"- Depth " + intToRoman(currentDepth));
                starCont++;
            }
            if(!planet.isStar())
                planet.setName("Lost planet - " + intToRoman(planetCont++));
        }
    }

    protected void handleJumpPoints(){
        for (SectorEntityToken jumpPoint : system.getJumpPoints()) {
            jumpPoint.setName("Escape point");
            jumpPoint.addTag("dive_escape");
            ((JumpPointAPI) jumpPoint).addDestination(new JumpPointAPI.JumpDestination(gauntletGate, "the in-system gate"));
            ((JumpPointAPI) jumpPoint).setStandardWormholeToHyperspaceVisual();
        }
    }

    //Getters
    public SectorEntityToken getGuide(){
        return guide;
    }

    public Dive_ThemeEnum getCurrentTheme() {
        return currentTheme;
    }

    public Dive_ThemeEnum.Subtheme getCurrentSubtheme() {
        return currentSubtheme;
    }

    public void setCurrentDepth(int currentDepth) {
        this.currentDepth = currentDepth;
    }

    public SectorEntityToken getGauntletGate() {
        return gauntletGate;
    }

    public Boolean getOverrideGuide() {
        return overrideGuide;
    }

    //Setters
    public void setOverrideGuide(Boolean overrideGuide) {
        this.overrideGuide = overrideGuide;
    }
    public void setGuide(SectorEntityToken guide) {
        this.guide = guide;
    }

    public void setCurrentSubtheme(Dive_ThemeEnum.Subtheme currentSubtheme) {
        this.currentSubtheme = currentSubtheme;
    }
}

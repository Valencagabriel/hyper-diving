package org.gabs.hyperdiving.impl.campaign.procgen;

import com.fs.starfarer.api.impl.campaign.procgen.StarAge;
import com.fs.starfarer.api.impl.campaign.procgen.StarSystemGenerator;

public class Dive_GenericParam {

    public static StarSystemGenerator.CustomConstellationParams systemParams(){
        StarSystemGenerator.CustomConstellationParams params = new StarSystemGenerator.CustomConstellationParams(StarAge.ANY);

        params.forceNebula = false;
        params.maxStars = 1;
        params.minStars = 1;
        params.name = "Faraway Sector";
        params.secondaryName = params.name;

        return params;
    }

}
